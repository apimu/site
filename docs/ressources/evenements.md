## Conférences, ateliers, séminaires ...

Quelques conférences qui pourraient être pertinentes

L'ATIEF à réalisé un classement des revues et conférences pertinentes pour la communauté EIAH en informatique et en sciences de l'éducation.

- L@S: Fourth Annual ACM Conference on Learning at Scale
- REV: International Conference Remote Engineering and Virtual Instrumentation
- ICALT: International Conference on Advanced Learning Technologies (IEEE)
- EIAH: Environnements Informatiques pour l'Apprentissage Humain
- EC-TEL: EC Technology Enhanced Learning
- AIED: artificial intelligence in EDucation
- ITS: Intelligent Tutoring System 
- IJCAI (y compris pour soumettre des démonstrations d'outils dans le “Call for demo”)
- AAMAS (y compris pour soumettre des démonstrations d'outils dans le “Call for demo”)
- International Conference on Software Engineering Education and Training (CSEET)
- ACM Conference on Innovation and Technology in Computer Science Education - [http://iticse.acm.org](http://iticse.acm.org) (y compris démonstrations d'outils, ateliers “working groups”, astuces de cours). 
- Koli Calling International Conference on Computing Education Research  - [http://www.kolicalling.fi/](http://www.kolicalling.fi/). Très rigoureux sur la science de l'éduc / évaluation / modèles explicatifs des outils proposes.
- International Conference on Informatics in Schools (ISSEP)
- IEEE-CS Conference on Software Engineering Education and Training (CSEE\&T)}
