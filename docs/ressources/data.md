## Données quantitatives sur l'Informatique en France
### (chiffres de 2015)

<br/>

* Au niveau des enseignants-chercheurs et chercheurs en France:

    - 3400 enseignants-chercheurs en section 27 CNU
    - 594 chercheurs CNRS INS2I + 989 chercheurs CNRS INSIS ([Bilan social 2015](http://bilansocial.dsi.cnrs.fr/pdf/BSP-2015.pdf), p14)
    - 615 chercheurs INRIA ([Bilan social 2015](https://fr.slideshare.net/INRIA/inria-bilan-social-2015), p16)
    - 1800 collaborateurs Inria (membres d'équipes Inria)

<br/>

* Au niveau des apprenants:

    - 12 millions d'élèves (~800.000 élèves par classe d'âge)
    - 65.000 établissements
    - 350.000 enseignants primaire
    - 500.000 enseignants secondaire
    - formation initiale – 16.000 recrutements secondaire 2016
