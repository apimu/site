## Évaluation d'outils d'apprentissage instrumenté de l'informatique


Note préliminaire : ce document démarre avec les réflexions menées à Font Romeu les 30 et 31 janvier, avec Fahima, Charles et Colinen particulier.


### Le point de départ est à peu près le suivant :

De nombreux instruments permettant d’apprendre à coder, programmer (mais plus généralement d’autres éléments d’informatique, voire d’autres éléments tout court). Pour que ces instruments puissent être appropriés, ils ne doivent pas seulement être excellents, ils doivent également « rassurer », c’est-à-dire convaincre de leur bien fondé, de la pertinence de l’approche. Il faut également montrer sur quels points particuliers il y a bénéfice, et, possiblement, pour quelle sous population.

Or pour convaincre, il faut pouvoir indiquer que les instruments ont été dûment inspectés, ou évalués.

Cela nous a conduit à proposer la création (ou plutôt l’identification) d’une taxonomie de l’évaluation. Dans un second temps cette taxonomie, enrichie, pourra être amenée à devenir une ontologie.

Les deux références trouvées (pour l’instant) ont été :

- A meta-study of algorithm visualization effectiveness . CD Hundhausen, SA Douglas, JT Stasko - Journal of Visual Languages & …, 2002 - Elsevier

- A Survey of Successful Evaluations of Program Visualization and Algorithm Animation System s . J Urquiza-Fuentes, JÁ Velázquez-Iturbide - Trans. Comput. Educ., 2009

Des versions en ligne accessibles des deux articles peuvent être trouvées sur le web.

Si on examine les deux taxonomies on trouve que celle de Hundhausen et al. est bien plus précise et documentée. Celle de Urquiza-Fuentes et Velázquez-Iturbide, plus récente, est subjective plutôt qu’objective (autrement dit la qualité de l’évaluation sert de critère de tri). [1]

<pre>
Hundhausen et al. proposent la taxonomie suivante.

Anecdotal techniques (3.1)
Experience report
Scenario walkthroughs
Time and effort estimates
Compelling examples
Programmatic techniques (3.2)
Analytic techniques (3.3)
Empirical techniques (3.4)
controlled experiments (3.4.1)
observational studies (3.4.2)
questionnaires and surveys (3.4.3)
ethnographic field techniques (3.4.4)
usability studies (3.4.5)
</pre>

Pour Urquiza-Fuentes, JÁ Velázquez-Iturbide, citons :

Informal evaluations. In this kind of evaluations, the students are typically asked their opinion after using the system. Descriptions of these evaluations often lack details of the process, materials or methodology used, reporting their results as positive/negative values.
Heuristic evaluations. These evaluations are performed by an expert, checking basic interactive features and also features specific to visualization systems.
Query techniques. This kind of evaluations consist in asking students, using questionnaires, about their opinion on different aspects of the system. The environment and the tasks performed by students are partially controlled by evaluators.
Observational studies. These evaluations differ from query techniques in the way information is collected. In this case, the evaluators observe how students use the system and write down everything they consider important for the evaluation.
Controlled experiments. In this evaluation type, the environment, the students and the tasks performed are highly controlled by evaluators. In addition to the students’ opinion, controlled experiments can provide information about effectiveness, ease of use, efficiency and other interesting issues.
Critique de ce que nous avons jusqu’à présent et suite.
A ce stade, la classification de Hundhausen est un excellent point de départ.

Il serait bien d’ajouter d’autres éléments méthodologiques (par exemple, quid des questionnaires pré-post ? Ce serait bien d’avoir un lien vers un papier méthodologique pour savoir quelles règles doit suivre une telle évaluation.)

Important : la taxonomie de Hundhausen et al. date de 2002. Or depuis, nous avons eu la montée en puissance d’internet et, au niveau éducatif des MOOCs. Il me semble que sa taxonomie pourrait avantageusement être corrigée en fonction de ces nouvelles technologies (mieux, une telle correction doit exister).

[1] Par mail, Jaime Urquiza Fuentes jaime.urquiza@urjc.es ajoute un pointeur sur la thèse de Hundhausen et quelques liens supplémentaires. Et suggère de regarder du côté des Learning Analytics pour savoir comment cette taxonomie peut bouger avec le temps. Ah si. Il ajoute également cette référence : Research Methods in Education ( [https://www.amazon.es/Research-Methods-Education-Louis-Cohen/dp/0415583365](https://www.amazon.es/Research-Methods-Education-Louis-Cohen/dp/0415583365) )