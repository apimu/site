# Contacts

Il est préférable de prendre contact via la liste de diffusion du groupe: [apimu@univ-lille.fr](mailto:apimu@univ-lille.fr)

Cependant, il est aussi possible de contacter individuellement les personnes qui étaient présentent lors de l'atelier AII/ORPHEE-RDV en 2017 à Font-Romeu ou qui ont rejoint le groupe par la suite :

* <i class="fa fa-envelope"> [Charles Boisvert](mailto:C.Boisvert@shu.ac.uk), Sheffield Hallam University</i>
* <i class="fa fa-envelope"> [Valerie Brasse](mailto:vbrasse@is4ri.com) Graines2Tech [Alsace]</i>
* <i class="fa fa-envelope"> [Julien Broisin](mailto:broisin@irit.fr), Université de Toulouse</i>
* <i class="fa fa-envelope"> [Moncef Daoud](mailto:moncef.daoud@univ-ubs.fr), Université de Bretagne Sud [Lorient]</i>
* <i class="fa fa-envelope"> [Fahima Djelil](mailto:fahima.djelil@imt-atlantique.fr), IMT Atlantique [Brest]</i>
* <i class="fa fa-envelope"> [Christine Ferrari](mailto:christine.ferrari@univ-savoie.fr), Université de Savoie</i>
* <i class="fa fa-envelope"> [Patrice Frison](mailto:patrice.frison@univ-ubs.fr), Université de Bretagne Sud [Vannes]</i>
* <i class="fa fa-envelope"> [Sébastien George](mailto:Sebastien.George@univ-lemans.fr), Université du Mans</i>
* <i class="fa fa-envelope"> [Colin de la Higuera](mailto:cdlh@univ-nantes.fr), Université de Nantes</i>
* <i class="fa fa-envelope"> [Martin Quinson](mailto:martin.quinson@ens-rennes.fr), ENS Rennes</i>
* <i class="fa fa-envelope"> [Christian Martel](mailto:Christian.Martel@univ-smb.fr), Université de Savoie</i>
* <i class="fa fa-envelope"> [Mathieu Muratet](mailto:mathieu.muratet@inshea.fr), INS-HEA</i>
* <i class="fa fa-envelope"> [Yannick Parmentier](mailto:yannick.parmentier@univ-lorraine.fr), Université de Lorraine</i>
* <i class="fa fa-envelope"> [Olivier Pons](mailto:olivier.pons@cnam.fr), CNAM</i>
* <i class="fa fa-envelope"> [Christian Queinnec](mailto:christian.queinnec@upmc.fr), Université Pierre et Marie Curie</i>
* <i class="fa fa-envelope"> [Yvan Peter](mailto:yvan.peter@univ-lille1.fr), Université de Lille - Sciences & Technologies</i>
* <i class="fa fa-envelope"> [Marine Roche](mailto:marine.roche1@univ-nantes.fr), Université de Nantes</i>
* <i class="fa fa-envelope"> [François Schwartzentruber](mailto:francois.schwarzentruber@ens-rennes.fr), ENS Rennes</i>
* <i class="fa fa-envelope"> [Yann Secq](mailto:yann.secq@univ-lille1.fr), Université de Lille - Sciences & Technologies</i>
* <i class="fa fa-envelope"> [Rémi Venant](mailto:remi.venant@irit.fr), Université de Toulouse</i>
* <i class="fa fa-envelope"> [Laurence Vignolet](mailto:laurence.vignollet@univ-savoie.fr), Université de Savoie</i>


