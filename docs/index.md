# Apprentissage de la Pensée Informatique de la Maternelle à l'Université (APIMU)

!!! tip "A propos"
    Le développement du numérique et des usages de l'informatique dans l'ensemble des champs professionnels et personnels confrontent les citoyens à des environnements relevant à de la “magie” pour un trop grand nombre d'entre eux. Avec la ré-introduction d'un enseignement des bases de l'informatique au lycée avec les options ICN et ISN et plus récemment au collège et au primaire, le défi de la massification de l'enseignement de l'informatique doit être relevé. Afin de passer à l'échelle de centaines de milliers d'élèves et dizaines de milliers d'enseignants, il nous semble nécessaire de fournir des environnements socio-techniques facilitant l'apprentissage de la pensée informatique tout au long de la vie et dans les différents cadres scolaires, périscolaire, familiaux et professionnels pour les enseignants et animateurs. <br/><br/>Ce site est né dans le cadre des [rencontres ORPHEE-RDV'17](http://www.orphee-edu.fr/orphee-rendez-vous-2017) et plus particulièrement dans l'atelier [apprentissage instrumenté de l'informatique](https://apprentissageinstrumentdelinformatique.wordpress.com/) et a pour but de synthétiser les ressources mobilisables pour l'initiation, l'appropriation, la transmission et la diffusion d'éléments de base de la pensée informatique. Initialement, le titre de ce site était Computational Teaching for Computational Thinking, mais il a été renommé pour mieux correspondre au cycle d'ateliers ayant été poursuivis ensuite. C'est avant tout un cahier de bord de l'activité de ce collectif informel, mais libre à chacun de s'en saisir.

!!! tip "Liste de diffusion"
    Une liste de diffusion (à faible débit) existe pour notre collectif informel et si vous souhaitez vous y inscrire, cliquez [ici](mailto:apimu@univ-lille.fr) :)

!!! tip "Actions passées et à venir"
	* [2025] Atelier [APIMU 1000](ateliers/eiah25.md) @ EIAH'25 le 10 juin 2025 à Lille (appel en cours)
    * [2024] Atelier commun GT compétences et GT APIMU [Modélisations de l’activité de programmation (savoirs et savoir-faire, compétences, capacités…) : de quelles manières ? pour quels besoins ? pour quels usages ?](ateliers/rjc-eiah24.md)
    * [2023] Atelier APIMU ["Mise à l'épreuve des dispositifs et outils"](ateliers/eiah23.md) @ EIAH'23 le 13 juin à Brest
    * [2022] Numéro spécial de la revue [Adjectif](http://www.adjectif.net/) : ["Quelles modalités novatrices d'enseignement de l'informatique"](http://www.adjectif.net/spip.php?article573)
    * [2021] Atelier ["Apprentissage de la pensée informatique de la maternelle à l’Université : retours d’expériences et passage à l’échelle"](ateliers/eiah21.md) à la conférence EIAH 2021 - 7 juin 2021 à Fribourg
    * [2021] Numéro spécial STICEF [“Technologies pour l’apprentissage de l’Informatique de la maternelle à l’université”](http://sticef.univ-lemans.fr/classement/appels.htm#tamiu)
    * [2020] [DidaPro 8](http://didapro.org/8) à Lille du 5 au 7 février ! :)
    * [2019] Atelier ["Organisation et suivi des activités d’apprentissage de l’informatique : outils, modèles et expériences"](ateliers/eiah19.md) à la conférence [EIAH 2019](https://eiah2019.sciencesconf.org/) à Paris
    * [2019] démarrage de l'activité du groupe de travail ["Apprentissage de l’Informatique de la maternelle à l’Université"](http://atief.fr/content/gt-apprentissage-informatique) au sein de l'[ATIEF](http://atief.fr/)
    * [2018] Atelier ["Organisation et suivi des activités d’apprentissage de l’informatique : outils, modèles et expériences"](ateliers/rjc-eiah18.md) à la conférence [RJC-EIAH 2018](http://atief.fr/sitesConf/rjceiah2018/rjceiah2018.html) à Besançon
    * [2017] Atelier ["Apprentissage de la pensée informatique de la maternelle à l’Université : recherches, pratiques et méthodes"](ateliers/aii-eiah17.md) à la conférence [EIAH 2017](https://eiah2017.unistra.fr/) à Strasbourg
    * [2017] Atelier ["Apprentissage Instrumenté de l'Informatique"](ateliers/aai17.md) - [Rencontre ORPHEE-RDV 2017](http://www.orphee-edu.fr) à Font-Romeu
