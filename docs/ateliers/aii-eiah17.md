# Atelier "Apprentissage de la pensée informatique de la maternelle à l'Université : recherches, pratiques et méthodes"

<br/>
## EIAH'17 - 6 juin 2017 - Strasbourg

<br/>
### Objectifs de l'atelier

À l'heure de la massification de l'enseignement de l'informatique, cet atelier a pour objectif de réunir chercheurs, enseignants et enseignants-chercheurs s'intéressant à l'usage d'artefacts numériques pour promouvoir l'apprentissage de la pensée informatique, de la maternelle à l'Université, à la fois pour les apprenants et leurs enseignants.

Il s'agit de présenter et mettre en débat les recherches et pratiques concernant l'apprentissage de la pensée informatique au moyen d'outils facilitant la compréhension et l'appropriation de concepts informatiques même complexes. Nous nous intéressons, sans restriction, à des outils et environnements tels que les exerciseurs interactifs, les jeux-sérieux, les micromondes, et tout dispositif pédagogique utilisant conjointement de tels environnements avec des robots éducatifs, des interfaces tangibles, ou autre MOOCs. Les contributions portant spécifiquement sur les outils, environnements ou pratiques pour l'enseignant (formation, accompagnement, aide au suivi de la classe, etc.) sont les bienvenues.

Un autre objectif de l'atelier est d'étudier et d'apporter des éléments de réponse aux verrous liés au passage à l'échelle des dispositifs et outils dédiés à l'apprentissage de l'informatique : comment s'y retrouver parmi la profusion de ressources disponibles ? Comment faciliter le passage d'une preuve de concept ou d'une expérimentation à petite échelle, à une exploitation par des centaines d'enseignants et quelques centaines de milliers d'élèves ?

Les contributions pourront être centrées sur un outil, un dispositif ou une expérimentation en étant attentif à décrire particulièrement les possibilités de diffusion et d'exploitation en masse.

<br/>
### Thèmes de l'atelier

Cet atelier pluridisciplinaire fait suite à l'atelier Orphée RDV « apprentissage instrumenté de l'informatique ». Il cherche à favoriser les échanges concernant les outils, expériences et pratiques autour de l'introduction de la pensée informatique et de la programmation.

Les thèmes attendus (non limitatifs) sont :

- Environnements numériques pour l'apprentissage de l'informatique
- Outils et visualisations pour l'analyse des apprentissages (à destination des apprenants, enseignants, tuteurs, concepteurs, etc.)
- Apprentissage massif de l'informatique
- Évaluation des outils, pratiques et méthodes d'apprentissage de l'informatique
- Formation / accompagnement des enseignants
- Ressources numériques libres

<br/>
### Soumission

Nous attendons des articles de 4 à 8 pages en PDF au format de la conférence EIAH (LNCS). Vous pouvez télécharger le modèle au format .docx ou Latex. Les articles feront l'objet d'une double relecture par les membres du comité scientifique.

Les articles seront à soumettre à l'adresse [ct2@univ-lille1.fr](mailto:ct2@univ-lille1.fr)

L'objectif est de contribuer au travail de recensement, d'organisation et d'évaluation des différentes ressources qui a été amorcé durant l'atelier qui s'est déroulé lors de Orphée RDV et d'envisager des convergences permettant une meilleure diffusion des résultats des travaux dans l'enseignement de la pensée informatique.

<br/>
### Dates

- soumission : 14 avril 2017 - *extension au 21 avril* !
- notification aux auteurs : 28 avril 2017
- référés détaillés : 5 mai 2017
- version définitive des articles : 19 mai 2017

<br/>
### Déroulement de l'atelier

L'atelier s'organise en deux temps: une session de présentation des différentes contributions le matin suivie d'une session de travail en groupe autours de thématiques centrales de l'atelier l'après-midi.

* 08h30-09h00	Accueil des participants

* 08h55-09h00	[Présentation de la journée](pdf-aii-eiah17/introduction_journee_apimu_eiah17.pdf)

* 09h00-09h50
    - *Les micro-mondes de programmation : état de l'art* ([Article](pdf-aii-eiah17/6-fahima-djelil-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/6-fahima-djelil-slides-apimu_eiah17.pdf)) Fahima DJELIL
    
    - *Apprendre la pensée informatique aux informaticiens* ([Article](pdf-aii-eiah17/14-julien-gossa-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/14-julien-gossa-slides-apimu_eiah17.pdf)) Julien GOSSA
    
    - *Un environnement visuel de programmation par flux de données pour soutenir l'apprentissage de la science des données* ([Article](pdf-aii-eiah17/4-charles-boivert-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/4-charles-boivert-slides-apimu_eiah17.pdf)) Charles BOISVERT, Konstantinos DOMDOUZIS, Matthew LOVE

* 09h50-10h40	*Ludification pour la motivation en apprentissage de la programmation* ([Article](pdf-aii-eiah17/8-sebastien-hoarau-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/8-sebastien-hoarau-slides-apimu_eiah17.pdf)) Sébastien HOARAU

    - *Apprentissage de la programmation en cycle 2 avec un jeu vidéo collaboratif* ([Article](pdf-aii-eiah17/12-christophe-reffay-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/12-christophe-reffay-slides-apimu_eiah17.pdf)) Christophe REFFAY, Frédéric DADEAU, Bruno FOLLET-LOCATELLI, Paul-Armand MICHAUD, Françoise GREFFIER

    - *Poppy Education: un dispositif robotique open source pour l'enseignement de l'informatique et de la robotique* ([Article](pdf-aii-eiah17/9-stephanie-noirpoudre-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/9-stephanie-noirpoudre-slides-apimu_eiah17.pdf)) Stéphanie NOIRPOUDRE, Didier ROY, Thibault DESPREZ, Théo SEGONDS, Damien CASELLI, Pierre-Yves OUDEYER

* 10h40-11h00	Pause

* 11h00-11h50

    - *Projets de robotique mobile à bac+2: des LEGO Mindstorm au robot fabriqué par soi même* ([Article](pdf-aii-eiah17/3-xavier-redon-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/3-xavier-redon-slides-apimu_eiah17.pdf)) Xavier REDON, Thomas VANTROYS, Alexandre BOÉ

    - *Apprentissage de la programmation à l'école par l'intermédiaire de robots éducatifs. Des environnements technologiques à intégrer* ([Article](pdf-aii-eiah17/5-olivier-grugier-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/5-olivier-grugier-slides-apimu_eiah17.pdf) + Vidéos [CE1 beebot](pdf-aii-eiah17/grugier-ce1-beebot-decouverte.mp4), [CE1 probot](pdf-aii-eiah17/grugier-ce1-pro_bot_car_programmation-pb_technique-modifie.mp4), [CE2 thymio](pdf-aii-eiah17/grugier-ce2-thymio-pb_capteur-modifie.mp4)) Olivier GRUGIER, François VILLEMONTEIX

    - *La CREP (Coupe de Robotique des Ecoles Primaires) : passerelle entre l'école primaire et l'école d'ingénieurs* ([Article](pdf-aii-eiah17/10-emmanuelle-pichonat-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/10-emmanuelle-pichonat-slides-apimu_eiah17.pdf)) Emmanuelle PICHONAT, Judith FRANÇOIS, Alexandre BOÉ, Isabelle MARECHAL, Walter HENNO

* 11h50-12h40

    - *Initiation à la pensée informatique avec le jeu de plateau Programming Boty* ([Article](pdf-aii-eiah17/11-lydie-boufflers-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/11-lydie-boufflers-slides-apimu_eiah17.pdf)) Lydie BOUFFLERS, Sophie LINH QUANG, Daniel K. SCHNEIDER
    
    - *Processus de transition de l'activité débranchée à la programmation avec AlgoTouch* ([Article](pdf-aii-eiah17/2-patrice-frison-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/2-patrice-frison-slides-apimu_eiah17.pdf) + [Synthèse](pdf-aii-eiah17/2-patrice-frison-synthese-apimu_eiah17.pdf)) Patrice FRISON, Moncef DAOUD

    - *CollKM for EdTech, une plateforme de support aux ateliers numériques* ([Article](pdf-aii-eiah17/7-valerie-brasse-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/7-valerie-brasse-slides-apimu_eiah17.pdf)) Valérie BRASSE, Laurent REMY, Thibaud JAMET

    - *Outils d'assistance et les difficultés d'enseignement/apprentissage de la programmation, quelle aide ?* ([Article](pdf-aii-eiah17/15-souleiman-ali-houssein-apimu_eiah17.pdf) + [Slides](pdf-aii-eiah17/15-souleiman-ali-houssein-slides-apimu_eiah17.pdf)) Souleyman ALI HOUSSEIN, Yvan PETER

* 13h00-14h00	Déjeuner

*  14h00-15h00	Échanges sur l'ensemble des sessions et [présentation des groupes de travail](pdf-aii-eiah17/introduction_groupe_travail_apimu_eiah17.pdf)

* 15h00-15h15	Courte pause

* 15h15-16h45	Deux groupes de travail en parallèle: “articles communs” ou “projets communs”

* 16h45-17h00	Courte pause

* 17h00-17h30	Retour des groupes de travail (15mn/atelier)

* 17h30-17h45	Bilan et fin de l'atelier

[Sauvegarde des Traces du pad de l'atelier sur le wiki](pdf-aii-eiah17/pad-eiah17.pdf)

[Lien pour l'inscription à EIAH](https://cloud.agoraevent.fr/Site/172901/1951/InscriptionPre?&AspxAutoDe%20tectCookieSupport=1)

<br/>
### Membres du comité scientifique

- Charles Boisvert (U. Sheffield Hallam)
- Valérie Brasse (IS4RI)
- Julien Broisin (U. Toulouse)
- Fahima Djelil (UHA)
- Julien Gossa (U. Strasbourg)
- Yvan Peter (U. Lille)
- Martin Quinson (ENS Rennes)
- Yann Secq (U. Lille)
- Rémi Venant (U. Toulouse)

<br/>
### Organisateurs de l'atelier

- Julien Broisin, E-C en Informatique à l'Université Paul Sabatier de Toulouse 3
- Julien Gossa, E-C en Informatique à l'Université de Strasbourg
- Yvan Peter, E-C en Informatique à l'Université de Lille - Sciences & Technologies (Lille 1)
- Yann Secq, E-C en Informatique à l'Université de Lille - Sciences & Technologies (Lille 1)
