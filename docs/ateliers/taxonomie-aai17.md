## taxonomie des outils existants

### Quelques surveys existants

[https://www.cs.cmu.edu/~caitlin/papers/NoviceProgSurvey.pdf](https://www.cs.cmu.edu/~caitlin/papers/NoviceProgSurvey.pdf)

Dans ce survey, la taxonomie est intéressante :
<pre>
 Teaching Systems
 Mechanics of programming
 Expressing programs
 Simplify typing code
 Find alternative to typing programs
 Structuring programs
 New programming models
 Making new models accessible
 Understanding program execution
 Make programming concrete
 Models of program execution
 Social learning
 Side by side
 Network of interaction
 Providing reasons to program
 Solve problems by positioning objects
 Solve problems using code
 Empowering systems
 Mechanics of programming
 Code is too difficult
 Improve programming languages
 Activities enhances by programming
 Entertainment
 Education
</pre>

- [État de l’art des outils de soutien à l’enseignement/apprentissage de la programmation](https://apprentissageinstrumentdelinformatique.files.wordpress.com/2016/06/aii1.pdf)
- [Programming Exercises Evaluation Systems: An Interoperability Survey](https://pdfs.semanticscholar.org/4a24/bfcc0bdd4aa1c80d2a9dca424d9fb60368ef.pdf) ([slides](http://www.dcc.fc.up.pt/~rqueiros/talks/CSEDU_2012.pdf))
- [Towards a Systematic Review of Automated Feedback Generation for Programming Exercises (2016)](https://pdfs.semanticscholar.org/6e86/ea437806f8bccd8372586b97e47f2e4467af.pdf)

#### Présupposés

1. On voudrait ne pas restreindre la taxo à l'algoprog
2. On se concentre sur les outils prévus pour un usage pédagogique (par exemple, on ignore Coq même si c'est utilisé en TP car à la base c'est pas fait pour ça)

#### Catégorie environnement de programmation pédagogique

1. Programmation recursive
    - [DrRacket](https://racket-lang.org/)
    - [Mooc Programmation recursive](http://programmation-recursive.net/)
2. Programmation Java
    - [Dr Java](http://www.drjava.org/)
    - [BlueJ](http://www.bluej.org/)
    - [Hop3x](https://hal.archives-ouvertes.fr/hal-00661515) = outil de suivi en temps réel de séances de TP java : dashboard pour l'enseignant pour une intervention plus efficace
3. Autres
    - [Processing](https://processing.org/)
    - [Prog&Play](https://www.irit.fr/ProgAndPlay)

#### Caractéristiques plus ou moins communes

- livre, videos, communauté
- installation simple ou en navigateur
- environnement de programmation, de mise au point,

#### Outil assistant pour apprendre un langage

- Logique
    - Assistants de preuve jouet
        - déduction naturelle
            - qui affiche des arbres: JaPE ([http://japeforall.org.uk/](http://japeforall.org.uk/)), Panda ([https://www.irit.fr/panda/](https://www.irit.fr/panda/)), Phox ([http://www.lama.univ-savoie.fr/~raffalli/phox.html](http://www.lama.univ-savoie.fr/~raffalli/phox.html)), PML ([http://www.lama.univ-savoie.fr/tracpml](http://www.lama.univ-savoie.fr/tracpml)), Why3, etc.
            - qui inclut un planificateur pour trouver une preuve : eremy Seligman and Declan Thompson. Teaching natural deduction in the right order with Natural Deduction Planner, Tools for teaching logic 2015. [http://sourceforge.net/p/proofassistant](http://sourceforge.net/p/proofassistant)
            - qui affiche du texte compréhensible par l'humain: Marek Materzok. Easyprove: a tool for teaching precise reasoning, Tools for teaching logic. 2015. ([http://easyprove.ii.uni.wroc.pl/](http://easyprove.ii.uni.wroc.pl/))
        - calcul des séquents: [http://logitext.mit.edu/main](http://logitext.mit.edu/main)
        - Méthode de tableaux: [http://creativeandcritical.net/prooftools](http://creativeandcritical.net/prooftools)
        - Spécialisé pour la géométrie (outil mort): [https://www-sop.inria.fr/lemme/Frederique.Guilhot/](https://www-sop.inria.fr/lemme/Frederique.Guilhot/)
    -  Assistants de preuve en vrai (détourné pour but pédagogique) : Coq ([https://coq.inria.fr](https://coq.inria.fr)), Isabelle ([https://isabelle.in.tum.de/](https://isabelle.in.tum.de/))
        - Adaptation de l'outil Isabelle à but pédagogique : Jørgen Villadsen, Alexander Birch Jensen and Anders Schlichtkrull. NaDeA: A Natural Deduction Assistant with a Formalization in Isabelle, Tools for teaching logic 2015.
    -  Résolution de contraintes
        - Solveur SAT
            - SAToulouse ([https://www.irit.fr/satoulouse/](https://www.irit.fr/satoulouse/)), outil qui affiche les étapes de calcul ([http://people.irisa.fr/Francois.Schwarzentruber/dpll_demo/](http://people.irisa.fr/Francois.Schwarzentruber/dpll_demo/)), etc.
        - SMT (SAT modulo theories) :
            - TouIST ([https://www.irit.fr/touist/](https://www.irit.fr/touist/) , [https://github.com/olzd/touist/releases](https://github.com/olzd/touist/releases)), Logic4fun ([https://l4f.cecs.anu.edu.au/](https://l4f.cecs.anu.edu.au/)), etc.
        - Logique modale : Kripke's world avec l'outil LoTREC ([https://www.irit.fr/Lotrec/](https://www.irit.fr/Lotrec/))
        - outil de spécification jouet : [http://alloy.mit.edu/alloy/](http://alloy.mit.edu/alloy/)
-  Outils qui présentent des micro-mondes (Tarski's world [https://ggweb.gradegrinder.net/tarskisworld](https://ggweb.gradegrinder.net/tarskisworld)), Hintikka's world ([http://people.irisa.fr/Francois.Schwarzentruber/hintikkasworld/](http://people.irisa.fr/Francois.Schwarzentruber/hintikkasworld/))
- Site web avec quelques exerciseurs (Logic in Action [logicinaction.org](https://logicinaction.org))
- Outil d'aide à la formalisation
    - *Using Automated Theorem Provers to Teach Knowledge Representation in First-Order Logic*, Angelo Kyrilov and David C. Noelle. Tools for teaching logic, 2015
    -  Livre avec des algorithmes SAT codés en CaML (livre de John Harrison, Handbook of Practical Reasoning)
    - Exerciseurs
        - Pour réécrire des formules de la logique propositionnelle : Josje Lodder, Bastiaan Heeren and Johan Jeuring. A pilot study of the use of LogEx, lessons learned, Tools for teaching logic, 2015.
    -  Utilisation de model checkers (plutôt cours de M1) : spin, nuSMV, etc.

#### Outils spécialisés pour l'enseignement de l'informatique théorique

- Langage formel
    - Transformation d'une grammaire algébrique en une grammaire en forme normale de Chomsky [http://compilation.irisa.fr/cnf/](http://compilation.irisa.fr/cnf/)
    - Algorithme CYK [http://compilation.irisa.fr/cyk/](http://compilation.irisa.fr/cyk/)
    - Calculabilité
        - Simulateur de machine de Turing ([http://people.irisa.fr/Francois.Schwarzentruber/turing_machine_simulator/](http://people.irisa.fr/Francois.Schwarzentruber/turing_machine_simulator/))
        - Catalogue de réductions ([http://people.irisa.fr/Francois.Schwarzentruber/reductioncatalog/](http://people.irisa.fr/Francois.Schwarzentruber/reductioncatalog/))
- Outils pour la maternelle: [http://maternellesmonique.eklablog.com/repertoire-d-applications-android-pour-la-maternelle-v3-a127061158](http://maternellesmonique.eklablog.com/repertoire-d-applications-android-pour-la-maternelle-v3-a127061158)

#### Catégories

- médias numériques,… (plusieurs catégories)
- des activités mathématiques autour des nombres, des formes
- des activités mathématiques autour de la logique
- des outils numériques pour coder le jouet programmable Bee-Bot. http://www.epi.asso.fr/revue/articles/a1204d.htm
- des outils numériques pour communiquer, collaborer
- [http://blog.codeweekfrance.org/apprendre-a-coder-outils-et-ressources-en-francais-1-4/](http://blog.codeweekfrance.org/apprendre-a-coder-outils-et-ressources-en-francais-1-4/) pas de catégories…, juste “pour qui?” enfants, ados, adultes
- cenralisée ici : [http://codeweekfrance.org/resources/](http://codeweekfrance.org/resources/) avec Age (enfant, ado, adulte) / Cible (atelier, solo, scolaire, périscolaire…) / Support (web, android, iOS…) / Langue (sans texte, français, anglais,…)
- [http://alternativeto.net/software/light-bot/](http://alternativeto.net/software/light-bot/) Catégories: licence (open source, free, commercial) / platform (Mac, Windows, Steam, Linux, Web/cloud, GitHub, BSD…) / customisability (No features added, Customizable)
- [http://app-enfant.fr/applications/tag/programmation/](http://app-enfant.fr/applications/tag/programmation/) Catégories: Age, Prix, Notes sur 5: Ambiance / Ergonomie / Educatif / Durée de vie / Globale, plateforme (iOS, Android), langues
- [http://eduscol.education.fr/jeu-numerique/#/article/1858](http://eduscol.education.fr/jeu-numerique/#/article/1858)
- Pour les petits toujours: [http://www.robotturtles.com/](http://www.robotturtles.com/) (jeu de plateau), la “prog” des 4ans
- Les produits de chez tralalère dès 8 ans par exemple : [http://www.tralalere.com/education/ressources/projets/gamecode-jeu-pour-apprendre-coder-creer-jeux-video](http://www.tralalere.com/education/ressources/projets/gamecode-jeu-pour-apprendre-coder-creer-jeux-video)
- Catégorisation possible selon les compétences visées (source complète de la SIF : [http://www.societe-informatique-de-france.fr/wp-content/uploads/2014/01/2014-01-CSP-programme-informatique-college.pdf](http://www.societe-informatique-de-france.fr/wp-content/uploads/2014/01/2014-01-CSP-programme-informatique-college.pdf))
    - Modéliser : utiliser un langage formel pour décrire un phénomène (exemple passer un nom au pluriel)
    - Abstraire : par exemple, en écrivant un programme qui permet de trouver la sortie d'un Labyrinthe, l’apprenant doit s'interroger sur la manière de représenter son labyrinthe. Il apprendre ainsi à abstraire une situation en gommant les détails non pertinents, à retrouver des invariants.
    - Projeter : un élève qui écrit un programme doit imaginer ce que ce programme fera
    - Réaliser : pour atteindre ces objectifs, il devra décomposer son projet en parties – par exemple un programme en fonctions, réaliser chaque partie, les valider (tests), identifier ses erreurs et les corriger. L'informatique est la technique où la boucle de test et de correction d'erreurs est la plus courte et la plus facile à mette en œuvre. L'erreur en informatique n'est, en outre, pas une faute qui dévalorise l'élève, mais une composante essentielle du processus de création.
    - Travailler en groupe : si les premiers exercices d'un élève sont souvent des activités individuelles, dès les premiers mois de l'apprentissage de l'informatique, les élèves réalisent des projets en groupe. Cela leur demande de négocier les objectifs et la répartition des tâches, d'expliquer leur travail et de comprendre le travail de leurs camarades.
    - Interagir avec un objet matériel : un élève n'écrit jamais un programme pour lui seul, ou pour son professeur, mais avant tout pour que ce programme soit exécuté par une machine, c'est-à-dire un objet matériel.
un outil d'apprentissage peut viser une ou plusieurs de ces compétences

##### Outils de visualisation: survey très complet de 2013
<pre>
@article{Sorva:2013:RGP:2543488.2490822,
  author = {Sorva, Juha and Karavirta, Ville and Malmi, Lauri},
  title = {A Review of Generic Program Visualization Systems for Introductory Programming Education},
  journal = {Trans. Comput. Educ.},
  issue_date = {November 2013},
  volume = {13},
  number = {4},
  month = nov,
  year = {2013},
  issn = {1946-6226},
  pages = {15:1--15:64},
  articleno = {15},
  numpages = {64},
  url = {http://doi.acm.org/10.1145/2490822},
  doi = {10.1145/2490822},
  acmid = {2490822},
  publisher = {ACM},
  address = {New York, NY, USA},
  keywords = {CS1, Introductory programming education, engagement taxonomy, literature review, notional machine, program dynamics, program visualization, software visualization},
}
</pre>

##### Taxonomies des environnements de programmation et langages pour débutant - 2005
<pre>
@article{Kelleher:2005:LBP:1089733.1089734,
  author = {Kelleher, Caitlin and Pausch, Randy},
  title = {Lowering the Barriers to Programming: A Taxonomy of Programming Environments and Languages for Novice Programmers},
  journal = {ACM Comput. Surv.},
  issue_date = {June 2005},
  volume = {37},
  number = {2},
  month = jun,
  year = {2005},
  issn = {0360-0300},
  pages = {83--137},
  numpages = {55},
  url = {http://doi.acm.org/10.1145/1089733.1089734},
  doi = {10.1145/1089733.1089734},
  acmid = {1089734},
  publisher = {ACM},
  address = {New York, NY, USA},
  keywords = {Human-computer interaction, computer Science education, learning, literacy, problem solving},
}
</pre>

##### Programmation par démonstration
<pre>
@article{Smith:2000:PEN:330534.330544,
  author = {Smith, David Canfield and Cypher, Allen and Tesler, Larry},
  title = {Programming by Example: Novice Programming Comes of Age},
  journal = {Commun. ACM},
  issue_date = {March 2000},
  volume = {43},
  number = {3},
  month = mar,
  year = {2000},
  issn = {0001-0782},
  pages = {75--81},
  numpages = {7},
  url = {http://doi.acm.org/10.1145/330534.330544},
  doi = {10.1145/330534.330544},
  acmid = {330544},
  publisher = {ACM},
  address = {New York, NY, USA},
}
@book{cypher1993watch,
  title={Watch what I Do: Programming By Demonstration},
  author={Cypher, Allen},
  year={1993},
  publisher={MIT Press}
}
@inproceedings{guibert2003teaching,
  title={Teaching and Learning Programming with a Programming by Example System},
  author={Guibert, Nicolas and Girard, Patrick},
  booktitle={International Symposium on End User Development},
  year={2003}
}
@inproceedings{smith1993pygmalion,
  title={Pygmalion: An executable electronic blackboard},
  author={Smith, David Canfield},
  booktitle={Watch what I do},
  pages={19--48},
  year={1993},
  organization={MIT Press}
}
@inproceedings{Frison2015,
  author = {Frison, Patrice},
  title = {A Teaching Assistant for Algorithm Construction},
  booktitle = {Proceedings of the 2015 ACM Conference on Innovation and Technology in Computer Science Education},
  series = {ITiCSE '15},
  year = {2015},
  isbn = {978-1-4503-3440-2},
  location = {Vilnius, Lithuania},
  pages = {9--14},
  numpages = {6},
  url = {http://doi.acm.org/10.1145/2729094.2742588},
  doi = {10.1145/2729094.2742588},
  acmid = {2742588},
  publisher = {ACM},
  address = {New York, NY, USA},
  keywords = {algorithm visualization, direct manipulation, novice programming environment, programming by demonstration},
}
</pre>

##### Comparaison des Travaux pratiques en classe et des TP à distance
<pre>
@article{Corter:2004bf,
  author = {Corter, J E and Nickerson, J V and Esche, S K and Chassapis, C},
  title = {{Remote versus hands-on labs: a comparative study}},
  journal = {Frontiers in Education {\ldots}},
  year = {2004},
  pages = {F1G--17--21 Vol. 2}
}
</pre>

##### Un historique intéressant de la didactique de l'informatique

[http://www.adjectif.net/spip/spip.php?article381](http://www.adjectif.net/spip/spip.php?article381)
