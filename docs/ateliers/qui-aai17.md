* **Martin Quinson**
    - tirer parti des learning analytics pour aider l'introduction de l'informatique
<br/><br/>

* **Patrice Frison & Moncef Daoud**
    - Comment passer de programmer avec les mains au programme sur machine
    - Lutter contre la page blanche, et au delà, comprendre comment on apprend à programmer
<br/><br/>

* **Colin de la Higuera**
    - Parcours personnel jusqu'à Class'Code
    - Comment la recherche peut-elle aider l'introduction de l'info ?
<br/><br/>

* **Olivier Pons**
    - Répondre à une baisse des effectifs
    - Comment organiser les cours d'intro dans ce contexte
<br/><br/>

* **Christian Queinnec**
    - Etablir une taxonomie des soumissions collectées (en terme de style, approche, méthode…)
    - Augmenter l'implication des étudiants
<br/><br/>

* **Mathieu Muratet**
    - Rencontrer la communauté
    - Mutualiser des projet / des technologies / des méthodologies d'évaluation
<br/><br/>

* **Yvan Peter**
    - Parvenir à tirer profit des learning analytics
    - Peut-on adapter notre dispositif pour le réseau ?
<br/><br/>

* **Yann Secq**
    - Comment enseigner la pensée informatique dès le plus jeune âge peut changer la facon d'apprendre les autres matières
    - Comment passer à l'echelle sur les initiatives locales d'enseignement
    - Constituer une communauté, vers un site central d'activités “sur étagère” (mais adaptable, ie. pas comme studio.code.org ;)) et de ressources pour différents types d'apprenants (élèves, étudiants, enseignants et citoyens)
    - Difficultés de création d'une dynamique impliquant différentes institutions: Rectorat (lien école/collège/lycée), les mairies (temps périscolaires), l'Université (mission de médiation scientifique peu reconnue, réussir à impliquer les étudiants), le milieu associatif et plus largement socio-économique)
<br/><br/>

* **Charles Boisvert**
    - Comment partager des outils artisanaux ?
        - pour qu'ils soient objets de recherche
        - pour qu'ils soient transférables
    - Comment évaluer les outils
        - besoin de faire du double aveugle?
        - ou bien des méta-études?
<br/><br/>

* **Sébastien George**
    - Simplifier le passage du débranché à la programmation traditionnelle
    - interactions avec objets tangibles
    - Structurer nos initiatives et contribution. Faire un site de référence ?
<br/><br/>

* **François Schwartzentruber**
    - Faire des outils à plus grande échelle, plutot autour de la logique
<br/><br/>

* **Christine Ferrari**
    - Apprentissage du code au plus tôt: vers un dispositif autour de PLM pour le primaire
<br/><br/>

* **Laurence Vignolet**
    - Quelles sont les compétences développées?
    - Attention à adapter les solutions à l'age: 8 ans vs. 18 ans
    - Référentiels de compétences de l'ACM et de l'europe (?)
    - Besoin d'aide pour détecter et comprendre les misconceptions des apprenants
    - Comment travailler sur les transitions entre les dispositifs
<br/><br/>

* **Valerie Brasse** (vbrasse@is4ri.com)
    - Entrepreneure sur des ateliers d'initiation au numérique dans les villages alsaciens (graine2tech)
    - Prendre du recul “recherche” sur les pratiques qui marchent
    - Comment passer à l'échelle ?
    - Monter des projets financés (expérience en montage de projets européens)
<br/><br/>

* **Marine Roche**
    - thèse en science de l'éducation avec Colin de la Higuera.
    - découvrir la communauté et les outils
<br/><br/>

* **Fahima Djelil**
    - Poursuivre les travaux engagés en thèse, au sein d'une communauté, surtout sur l'évaluation des outils
<br/><br/>

* **Christian Martel**
    - Faire la jonction entre les outils qu'on fait pour nous et les ENT que tout le monde utilise
<br/><br/>
