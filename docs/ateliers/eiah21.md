# Atelier "Apprentissage de la pensée informatique de la maternelle à l'Université : retours d'expériences et passage à l'échelle"

<br/>
## EIAH 2021 - Lundi 7 juin 2021, Fribourg (en visio)

<br/>
### Descriptif de l'atelier

Depuis une vingtaine d'années, l'enseignement de l'informatique n'était plus réalisé que dans le supérieur. Un mouvement de réintroduction de l'informatique dans le contexte scolaire en primaire et secondaire s'est produit à différents rythmes dans certains pays anglo-saxons et européens depuis une dizaine d'années. Depuis 5 ans environ, en France et plus récemment en Suisse, ces enseignements ont trouvé une part encore plus importante dans l'ensemble des cycles suite à leur inscription dans les programmes scolaires. Cette introduction massive de l'enseignement de l'informatique dans les programmes scolaires induit des problématiques cruciales en termes de didactique d'une discipline principalement enseignée dans le supérieur ces dernières années, et de formation des enseignant·e·s qui dispensent ces cours dans le primaire et le secondaire.

Cet atelier qui poursuit les sessions organisées depuis 2017 ([Journées Orphée RDV 2017](https://apprentissageinstrumentdelinformatique.wordpress.com/actes/), [EIAH 2017](aii-eiah17.md), [RJC'EIAH 2018](rjc-eiah18.md), [EIAH 2019](eiah19.md)) vise à fédérer une communauté de chercheurs et chercheuses issu·e·s de différents champs disciplinaires autour de la problématique de l'apprentissage de la pensée informatique de la maternelle à l'université afin de bâtir une vision commune du domaine, d'identifier les principales problématiques et de proposer des recherches et ressources pour développer la didactique de l'informatique et sa diffusion à large échelle auprès des enseignant·e·s.

Comme lors des sessions précédentes, les contributions attendues devront principalement porter sur les axes suivants :

- Apprendre et enseigner la pensée informatique,
- Former et accompagner les enseignant·e·s,
- Produire, évaluer et diffuser des outils et des ressources éducatives.

Les contributions qui accordent une attention particulière aux problématiques liées à l'instrumentation de ces apprentissages sont encouragées et une attention particulière est apportée aux contributions s'intéressant au déséquilibre de genre en informatique. D'autre part, des retours d'expérience sur la mise en œuvre des nouveaux programmes scolaires intégrant l'apprentissage de l'informatique sont les bienvenus.

Cet atelier sera co-organisé pour la seconde fois avec le projet [ANR IE CARE](http://iecare.lip6.fr/) (enseignement de l'informatique à l'école obligatoire, 3-16 ans) et le projet [ERASMUS+ PIAF](https://piaf.loria.fr) (Pensée Informatique et Algorithmique dans l'enseignement Fondamental) offrant l'opportunité d'un regard croisé sur les enjeux de cette massification de l'enseignement de l'informatique du plus jeune âge jusqu'aux adultes.

<br/>
### Organisation de la journée

Matinée : APIMU : présentation d'articles et discussions

* 9h00 à 9h15 : Accueil

* 9h15 à 10h15 : **Session 1 : “Enseigner l'informatique à l'école primaire”**

     - [*Hypothèse de la « distance » appliquée à la robot-pédagogie pour les enfants en maternelle*](https://hal.science/hal-03241690), Julian Alvarez, Katell Bellegarde, Julie Boyaval, Vincent Hurez, Jean-Jacques Flahaut et Thierry Lafouge (["bande annonce" (vidéo)](pdf-apimu21/apimu_21_alvarez-bellegarde.mp4))

     - [*Machine Notionnelle et Pratiques de la Programmation: Mieux Apprendre avec le Développement Progressif*](https://hal.science/hal-03241694), Charles Boisvert

     - [*Pensée informatique et activités de programmation : quels outils pour enseigner et évaluer ?*](https://hal.science/hal-03241689), Kevin Sigayret, Nathalie Blanc et André Tricot

     - [*Une approche méta-design du jeu sérieux pour l'enseignement de l'informatique à l'école élémentaire*](https://hal.science/hal-03241691), Xavier Nédélec, Bertrand Marne, Mathieu Muratet, Karim Sehaba et Jean Lapostolle

* 10h15 à 10h30 : Pause

* 10h30 à 11h15 : **Session 2 : “Former les enseignant·e·s”**

     - [*Apprendre à penser les algorithmes*](https://hal.science/hal-03241685), Christian Blanvillain. Apprendre à penser les algorithmes ([support (PDF)](pdf-apimu21/apimu21_support-c.blanvillain.pdf) et ["bande annonce" (vidéo)](https://www.youtube.com/watch?v=FtPSA5T6rM4))

     - [*Donner du sens à l'objet numérique dans la formation des futur·e·s professeur·e·s des écoles*](https://hal.science/hal-03241686), Yannick Parmentier et Sylvie Kirchmeyer

     - [*Changer la représentation de l'informatique chez les jeunes : recommandations*](https://hal.science/hal-03241692), Julie Henry, Cécile Lombart et Bruno Dumas. Changer la représentation de l'informatique chez les jeunes : recommandations

* 11h15 à 12h00 : **Session 3 : “Enseigner l'informatique dans le supérieur”**

     - [*Construction d'un programme combinant exécution partielle et manipulation directe*](https://hal.science/hal-03241687), Adam Michel, Daoud Moncef et Patrice Frison (["bande annonce" vidéo](https://www.youtube.com/watch?v=jvocXctEP7A))

     - [*PseuToPy: Vers un langage de programmation naturel*](https://hal.science/hal-03241688), Yassine Gader, Charles Lefever et Patrick Wang

     - [*Permettre l'évaluation par les pairs de projets tuteurés en informatique : une grille critériée adaptée aux jeux sérieux*](https://hal.science/hal-03241693), Ying-Dong Liu, Julien Gossa et Laurence Schmoll

Après-midi : APIMU et HUMANE

* 13h30 à 14h30 : Table ronde commune entre les ateliers APIMU et [HUMANE](https://humane-eiah-21.sciencesconf.org/)

* 15h00 à 17h00 : Possibilité d'assister à l'[atelier HUMANE](https://humane-eiah-21.sciencesconf.org/) dans la foulée de la table ronde pour les collègues intéressé·e·s

<br/>
### Actes

Les actes de l'atelier en PDF sont disponibles en suivant [ce lien (HAL)](https://hal.science/hal-03241714) ou les liens directs vers chacune des contributions dans le programme ci-dessus.

<br/>
### Inscription à l'atelier

L'inscription à l'atelier est gratuite, mais doit être réalisé via le formulaire de la conférence : [https://blog.hepfr.ch/eiah2021/inscription-a-eiah2021](https://blog.hepfr.ch/eiah2021/inscription-a-eiah2021)

<br/>
### Calendrier et soumissions

- Date limite de soumission des contributions : ~~18 avril 2021~~ 23 avril 2021
- Date de notification aux auteur·e·s : ~~26 avril 2021~~ 30 avril 2021
- Dépôt de la version définitive : ~~7 mai 2021~~ 10 mai 2021
- Publication sur ce site : ~~15 mai 2021~~ 21 mai 2021
- Journée de l'atelier dans le cadre d'[EIAH'21](https://blog.hepfr.ch/eiah2021) : lundi 7 juin toute la journée !

<br/>
### Informations aux auteur·e·s

[Archive contenant les patrons en différents formats (odt, docx, tex)](apimu_styles.zip)
En fonction de la nature de votre contribution, merci de rédiger un article compris entre 4 et 8 pages au maximum.

Il n'est pas nécessaire d'anonymiser votre contribution, sauf si vous le souhaitez :)

Merci de suivre ce [lien pour déposer votre contribution (APIMU'21@easychair.org)](https://easychair.org/my/conference?conf=apimu2021)

<br/>
### Collègues impliqués dans l'organisation d'APIMU

#### Comité d'organisation
* Julien BROISIN (Université de Toulouse)
* Christophe DECLERCQ (INSPÉ de Nantes)
* Cédric FLUCKIGER (Université de Lille)
* Yannick PARMENTIER (Université de Lorraine)
* Yvan PETER (Université de Lille)
* Yann SECQ (Université de Lille)

#### Comité scientifique
* Georges-Louis BARON (Université de Paris)
* Isabelle COLLET (Université Genève)
* Brigitte DENIS (Université de Liège)
* Liesbeth DE MOL (Université de Lille)
* Marie DUFLOT-KREMER (Université de Lorraine)
* Colin de la HIGUERA (Université de Nantes)
* Bern MARTENS (Universiteit Leuven)
* Gabriel PARRIAUX (HEP Lausanne)
* Robert REUTER (Université du Luxembourg)
* Margarida ROMERO (Université Côte d'Azur)
* Thierry VIÉVILLE (Inria Sophia Antipolis)

#### Comité de lecture
* Florent BECKER (Université Orléans)
* Charles BOISVERT (Université Sheffield)
* Laure BOLKA-TABARY (Université de Lille)
* Kris COOLSAET (Universiteit Gent)
* Yannis DELMAS (Université de Poitiers)
* Fahima DJELIL (IMT Atlantique)
* Béatrice DROT-DELANGE (Université de Clermont-Ferrand)
* Olivier GOLETTI (Université de Louvain)
* Monique GRANDBASTIEN (Université de Lorraine)
* Pascal LEROUX (Le Mans Université)
* Kim MENS (Université de Louvain)
* Christophe REFFAY (Université Franche-Comté)
* Eric SANCHEZ (TECFA, Université de Genève)
