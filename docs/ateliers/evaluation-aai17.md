## Groupe "évaluation des outils"

- Groupe “évaluation des outils” (carte mentale: [https://framindmap.org/c/maps/302940/edit](https://framindmap.org/c/maps/302940/edit) )

    1. focalisation sur l'évaluation des outils et un peu des apprenants

    2. Un objectif serait de classifier les méthodes d'évaluation des outils, pour simplifier les méta-études

    3. Problème similaire est d'avoir des corpus de données réutilisables

    4. Il faudrait prendre l'évaluation de l'outil en compte lors de sa conception. Et pour bien monter dans les niveaux méta, les outils dont on parle permette d'évaluer les apprenants

- **Evaluation** - quelles methodes?

    1. besoin d'interdisciplinarite

    2. methodes des sciences humaines - qual/quant… -

    3. outils d'evaluation de nos outils d'apprentissage (!!)

- quels buts

    1. evaluer les apprentissages?

    2. evaluer les outils eux-memes (l'evaluation des apprentissages est un moyen d'evaluation des outils)

    3. donner des resultats d'evaluation aux enseignants? - soutenir le travail reflexif

- l'evaluation comme moyen de conception

    1. evaluer tot - des prototypes ou meme des idees )

    2. evaluer pour reviser, agilite

- difficile contexte de l'evaluation

    1. but des outils → but de l'evaluation

         - en termes de competences a acquerir

         - de methodes utilisees

         - de niveaux des publics…
         
    2. diversite - l'evaluation permet peu de comparaisons

         - on manque de references (comme sont les corpus des sciences humaines) - qui aiderait le praticien et le chercheur a repondre a ses questions

         - besoin de rendre les evaluations comparables (la ou c'est possible)

Parcourir ce document de la SIF : [Enseigner l'informatique de la maternelle à la terminale](http://www.societe-informatique-de-france.fr/wp-content/uploads/2016/10/1024-no9-Enseigner-linformatique-de-la-maternelle-%C3%A0-luniversite-a-paraitre.pdf), et pour chaque concept (algorithme, machine, langage, information), définir les outils qui pourraient être utilisés et identifier les éventuels manques (verrous?)

**Resume apres coup** (lundi apres midi)

- Une taxonomie ou ontologie pour caracteriser les evaluations

    1. qui incluerait

          - methodes

          - des résultats dans leur contexte

          - des outils (d'evaluations)

    2. qui serait liee a une taxonomie ou ontologie des instruments d'apprentissage (IA)

          - objectifs des IA

          - contexte d'usage

              - niveau des apprenants

              - age

              - etc
              
    3. moyens techniques - quelle plate-forme…

    4. exemple: un prof se demande - PLM ou Scratch?

           - trouver les objectifs

               - certains sont communs

               - pas tous

           - recherche des evaluations

               - qui sont en fonctions des objectifs

               - comparer, la ou la comparaison est possible
               
    5. est-ce qu'une recherche automatisee des evaluations peut devenir elle-meme un moyen d'evaluation
