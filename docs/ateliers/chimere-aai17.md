## Spécifications fonctionnelles de notre chimère

Partie Plateforme (portail) de gestion de connaissances / ressources sur l'apprentissage/l'enseignement de l'informatique (pensée informatique)

#### Contexte et présupposés

Imaginons un enseignant avec 25 élèves dans une classe. Que peut-on lui offrir pour qu'il puisse facilement mettre en place des activités d'enseignement de l'informatique dans sa classe.

Quelles fonctionnalités “dans l'absolu” de notre chimère ?

#### avant

- l'enseignant peut s'auto-former (est-ce lié directement ou pas aux séquences pédagogiques ?), ressources spécifiques pour la formation d'enseignants
- l'enseignant dispose d'un référentiel des ressources / des séquences / des compétences
- On peut référencer des ressources nouvelles
- On dispose d'un moteur de recherche, interrogeable sur les différentes dimensions
- l'enseignant peut créer ses propres séquences pédagogiques
- l'enseignant peut adapter les séquences pédagogiques
- l'enseignant peut définir ses propres observables (ou sélectionner parmi ceux proposés ?) et les formes de visualisation (tableau de bord) → question sur les standards à ce niveau
- l'enseignant peut choisir les types de visualisation des observables (personnalisation du tableau de bord de l'enseignant)
- des solutions de déploiement/d'opérationnalisation (faciliter la phase d'installation de logiciel, création de comptes, impression pour activité débranchée, …)
- opérationalisation = l'enseignant décide du matériel pédagogique nécessaire pour sa séquence

déploiement = chaque élève a le matériel devant lui

#### pendant

- L'apprenant utilise un environnement d'apprentissage instrumenté (IDE, tangible, …), ie. pouvant produire des observations/traces
- Les traces sont collectées pour pouvoir être interrogées après coup et ou en direct pour le tableau de bord (supervision durant la séance)
- Tous les intervenants ont des usages spécifiques de ces traces (apprenant, enseignant, auteur, chercheur)
- l'environnement d'apprentissage doit être observable (niveau 0: traces d'interactions/d'usages, niveau 1: observations de l'enseignant, analyse de la production de l'élève, …)
- l'enseignant doit pouvoir visualiser la progression de la classe (cf. tableau de bord)
- l'enseignant doit pouvoir intervenir pour aider un élève (suivi pédagogique) ou pour debriefer avec la classe en cours d'activité
- l'enseignant doit pouvoir garder et partager les productions des élèves (programmes, artefacts,…)
- L'environnement donne un feedback voire une remédiation aux actions des apprenants
- l'apprenant reçoit des propositions d'activités complémentaires
- L'apprenant peut demander de l'aide

#### après

- l'enseignant peut donner un feedback sur la (ré-)utilisation des ressources, des séquences, des outils
- affiner le référentiel par rapport aux contextes d'usage

#### Tout au long du processus

- Les enseignants disposent d'un moyen de communication entre eux et avec les concepteurs (forum, communauté de pratique)
- Les acteurs sont authentifiés et identifiés

#### Etapes permettant de s'approcher de l'utopie

- Recenser ce qui existe déjà
- mécanisme de veille automatique de ressources (harvesting et text mining de pages web, flux de réseaux sociaux, etc)

#### Quels challenges ?

- Comment former aux bases de l'informatique l'ensemble des apprenants de la maternelle à l'Université ?
    - cible primaire: les élèves et étudiants
    - cible secondaire: professeurs des écoles et de collèges et lycées
- Quelles bases de l'informatique ?
    - cible primaire: bases de l'algorithmique et de la programmation
    - cible secondaire: système, réseau, BDD, …
- Comment passer à l'échelle sur la formation des encadrants / enseignants / médiateurs ?
    - 86100 enseignants dans les écoles (primaires ?) + collège + lycées
    - 71 universités (en 2016) et 4200 lycées (dont 1500 pro), 7100 collèges (rentrée 2015) et 51700 écoles élémentaires
- nécessité d'un environnement / d'une infrastructure instrumentée et problématique de son appropriation par les enseignants

#### présupposés / questions

- 4 rôles (une personne peut avoir plusieurs rôles!): Enseignant, Apprenant, Concepteur de ressources, Chercheur
- une infrastructure logicielle permettant
    - aux usagers d'être authentifiés / identifiés (fédération d'identité),
    - de stocker / partager des informations
    - d'interagir avec d'autres usagers (détails ci-dessous aspect réseau social)
    - infrastructure “ouverte” et accessible facilement (cf. API REST …)
- nécessité d'un accompagnement en terme de formation continue des enseignants (exploitant les OER et l'infrastructure)
- nécessité d'un travail institutionnel pour expérimenter à large échelle et favoriser l'émergence d'un réseau de collègues impliqués dans l'utilisation (niveau 1) et la création d'OER (niveau 2)
- à quel point souhaite-t-on pouvoir intégrer “n'importe quelle” ressource ? (ex: discussion sur la difficulté d'intégration/instrumentation de Scratch)
- intégration via les ENT ?

#### fonctionnalités liées à l'ingénierie pédagogique

- organiser / catégoriser les Open Educational Resources (OER):
- référencer les outils / instruments avec une taxonomie / ontologie / un référentiel précis
- trouver un “sweet spot” / équilibre entre une ontologie type OWL trop détaillée qui ne sera pas appropriée et un système d'étiquettes/tags trop simpliste ne permettant pas des recherches suffisamment précise pour être pertinentes
- piste pour les concepts principaux liés à l'informatique : [http://www.societe-informatique-de-france.fr/wp-content/uploads/2016/10/1024-no9-Enseigner-linformatique-de-la-maternelle-%C3%A0-luniversite-a-paraitre.pdf](http://www.societe-informatique-de-france.fr/wp-content/uploads/2016/10/1024-no9-Enseigner-linformatique-de-la-maternelle-%C3%A0-luniversite-a-paraitre.pdf)
- premier découpage conceptuel et associé à des “niveaux” permettant d'associer les outils pouvant être utilisés et d'identifier d'éventuels manque (next step: possibilité d'intégration de ces outils)
- proposition de catégorisation possible selon les compétences visées (source SIF) : [http://www.societe-informatique-de-france.fr/wp-content/uploads/2014/01/2014-01-CSP-programme-informatique-college.pdf](http://www.societe-informatique-de-france.fr/wp-content/uploads/2014/01/2014-01-CSP-programme-informatique-college.pdf)
    - Modéliser: utiliser un langage formel pour décrire un phénomène (exemple passer un nom au pluriel)
    - Abstraire: par exemple, en écrivant un programme qui permet de trouver la sortie d'un Labyrinthe, l’apprenant doit s'interroger sur la manière de représenter son labyrinthe. Il apprendre ainsi à abstraire une situation en gommant les détails non pertinents, à retrouver des invariants.
    - Projeter: un élève qui écrit un programme doit imaginer ce que ce programme fera
    - Réaliser: pour atteindre ces objectifs, il devra décomposer son projet en parties – par exemple un programme en fonctions, réaliser chaque partie, les valider (tests), identifier ses erreurs et les corriger. L'informatique est la technique où la boucle de test et de correction d'erreurs est la plus courte et la plus facile à mette en œuvre. L'erreur en informatique n'est, en outre, pas une faute qui dévalorise l'élève, mais une composante essentielle du processus de création.
    - Travailler en groupe: si les premiers exercices d'un élève sont souvent des activités individuelles, dès les premiers mois de l'apprentissage de l'informatique, les élèves réalisent des projets en groupe. Cela leur demande de négocier les objectifs et la répartition des tâches, d'expliquer leur travail et de comprendre le travail de leurs camarades.
    - Interagir avec un objet matériel: un élève n'écrit jamais un programme pour lui seul, ou pour son professeur, mais avant tout pour que ce programme soit exécuté par une machine, c'est-à-dire un objet matériel.
- construire un curriculum complet de l'informatique, de 5 à 20 (90!) ans
    - pour le supérieur
        - Computer Science Curricula 2013 Curriculum Guidelines for Undergraduate Degree Programs in Computer Science
        - Programme Pédagogique National DUT Informatique
    - pour le secondaire
        - au lycée: en seconde et première [Informatique et Création du Numérique](http://cache.media.education.gouv.fr/file/CSP/91/2/prog_Informatique_et_creation_numerique_19_mai_425912.pdf) (ICN) et en terminale [Informatique et Science du Numérique](http://eduscol.education.fr/cid59678/presentation.html) (ISN)
        - [programme au collège pour les enseignant de maths](http://cache.media.education.gouv.fr/file/special_6/52/5/Programme_math_33525.pdf)
        - pour les primaires: [1, 2, 3 codez de la Fondation la Main à la pâte](http://www.cafepedagogique.net/LEXPRESSO/Pages/2016/06/03062016Article636005337950833622.aspx)
- référencer les cours / séances pédagogiques (OER) à un niveau de granularité fine avec pré-requis et compétences développées
- réutilisation d'OER
- faciliter la recherche (par facettes, filtres …) d'activités/de ressources pour les réutiliser afin de construire de nouvelles séquences pédagogiques
- naviguer parmi les ressources: liens entre références de séances et références d'outils
- disposer d'un système de recommandations au niveau fin des OER
- niveau d'abstraction de la notion de “séquence pédagogique”
    - abstrait: description abstraite non liée à l'opérationalisation
    - opérationnel: description d'un enchaînement d'activités centrés sur l'apprentissage d'une notion spécifique

#### fonctionnalités favorisant l'évaluation des usages

- permettre l'observabilité des usages des différentes activités et ressources
    - “facile” lors de l'usage d'un logiciel par l'apprenant
    - activités tangibles: observations possibles aussi :)
    - par contre, pour les activités débranchées: pas d'observation automatisée (ou difficilement !)
- définir un ensemble de métrique de la plus basique à la plus évoluée:
    - métriques centrées individus: temps passé sur les différentes parties d'une séquence pédagogique, nombre d'interactions avec d'autres apprenants
    - métriques centrées classes: progression moyenne des apprenants sur les différentes parties d'une SP
    - métriques centrées cohortes (classe d'âge/niveau): niveau d'agrégation supplémentaire sur tous les 6ème d'une académie utilisant l'infrastructure
- produire un tableau de bord (basé sur les observables) personnalisable par l'enseignant permettant de superviser/avoir une vue globale de l'activité des apprenants durant les séances
- problématique de la diffusion et l'exploitation des traces d'usages, cf. anonymisation des données:
    - niveau 1: pas d'information personnelle (juste les traces liées à l'activité pour un usager n'étant qu'un identifiant unique et des informations agrégées) ⇒ ouvert à tous
    - niveau 2: information “collective” (information de contexte comme l'école, le niveau des apprenants) ⇒ au niveau de la “communauté éducative”
    - niveau 3: information individuelle (âge, sexe)) ⇒ sur “accréditation” (cf. comme certaines données d'enquêtes de l'INSEE)

#### fonctionnalités liées à l'apprentissage de la programmation

- fournir des ressources d'initiation à la pensée informatique avec différentes granularités et différentes formes (aussi observables !)
- proposer différents formalismes d'expression d'algorithmes (ex: cartes d'action, programmation par bloc, programmation textuelle, …)
- fournir des aides à la résolution d'exercices lorsqu'un apprenant est en difficulté (exploitation des différentes métriques)
- différents types d'activités liées à l'apprentissage de la programmation:
    - résolution de problèmes en contexte restreint: déplacement d'un personnage sur une grille, tour de hanoï, …
    - compréhension d'algorithmes existants: trouver l'algorithme correspondant à un traitement, trouver un bug dans un algo donné, …
    - résolution de problèmes en contexte ouvert: modélisation / décomposition d'un problème donné (cf. COO)
    - programmation créative: pas d'objectif à priori, création d'animations sous Scratch, création d'oeuvres numériques avec Processing …

#### fonctionnalités favorisant l'émergence d'un réseau social des usagers de l'outil

- favoriser l'émergence d'une communauté d'enseignants / communauté de pratiques:
    - forum de discussions
    - système de questions/réponses type [StackOverflow](https://en.wikipedia.org/wiki/Stack_Overflow)
- va-t-on jusqu'aux temps en présentiel (par exemple, les temps de rencontre dans Class'Code) ?
- permettre aux apprenants de partager leurs productions et commenter les solutions des autres
- réseau social avec un système de like ou points à définir (point +/- selon un contexte, une audience, un pré-requis, etc, pas juste en soi) → collaboration entre enseignants
- exploitation de mécanismes de gamification pour maintenir la dynamique/la motivation des apprenants

#### Périmètre additionnel

- intégrer les outils (API) pour récupérer des traces et faire des Learning Analytics
- personnaliser l'exécution d'une séquence en fonction de l'échec / succès de l'apprenant
- intégration avec ENT dans les collèges / lycées?
- élargir audience à concepteurs d'outils, et évaluateurs d'outils
- public 2 : les concepteurs d'outils ou de ressources
- comment décrire les contributions (méta-données) pour les insérer dans la plate-forme
- comment évaluer (aspect méthodologique)
- public 3 : les élèves ?
- faire des activités en dehors de la classe ?
