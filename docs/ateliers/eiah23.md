# Atelier "Apprentissage de la pensée informatique de la maternelle à l'Université : mise à l'épreuve des dispositifs et outils"

<br/>
## EIAH 2023 - Mardi 13 juin 2023, Brest

<br/>
### Descriptif de l'atelier

!!! tip ""
    Cet atelier, organisé par le Groupe de Travail APIMU (Apprentissage de l'Informatique de la Maternelle à l'Université) fait suite aux ateliers organisés en 2017 lors des [journées Orphée RDV “apprentissage instrumenté de l'informatique”](https://apprentissageinstrumentdelinformatique.wordpress.com/actes/), puis lors des conférences [EIAH 2017](aii-eiah17.md), [2019](eiah19.md), [2021](eiah21.md) et lors des [RJC-EIAH 2018](rjc-eiah18.md). <br/>Il vise à fédérer une communauté de chercheuses et de chercheurs issu·e·s de différents champs disciplinaires autour de la problématique de l'apprentissage de la pensée informatique afin de bâtir une vision commune du domaine, en partager les problématiques et envisager les solutions possibles. <br/>Pour cet atelier, nous souhaitons mettre l'accent sur la **mise à l'épreuve des dispositifs et outils** proposés : dispositifs expérimentaux, analyses qualitatives et quantitatives des outils et dispositifs mis en œuvre et leurs résultats. Nous sommes particulièrement intéressés par les environnements collaboratifs qui semblent peu représentés dans les travaux existants [1].<br/><br/>Cet atelier sera co-organisé pour la troisième fois avec le projet [ANR IE CARE](http://iecare.lip6.fr/) (enseignement de l'informatique à l'école obligatoire, 3-16 ans). <br/><br/>[1] Lai, X., & Wong, G. K.-W. (2022). Collaborative versus individual problem solving in computational thinking through programming: A meta-analysis. British Journal of Educational Technology, 53, 150– 170. [https://bera-journals.onlinelibrary.wiley.com/doi/10.1111/bjet.13157](https://bera-journals.onlinelibrary.wiley.com/doi/10.1111/bjet.13157)

<br/>
### Thèmes

!!! tip ""
    Les thèmes qui pourront être abordés incluent :
    
    * Environnements numériques pour l'apprentissage de la pensée informatique : micro-mondes, tuteur intelligent, jeux sérieux, robotique, interfaces tangibles…
    
    * Outils et visualisations pour le suivi et l'analyse des apprentissages (à destination des apprenants, enseignants, tuteurs, concepteurs, etc.)
    
    * Modélisation et organisation des activités, séquences et scénarios pédagogiques
    
    * Outils de remédiation, de rétroaction, de régulation pour l'enseignant et/ou l'apprenant
    
    * Environnements collaboratifs :  usage d'environnements collaboratifs de développement logiciel ou instruments conçus spécifiquement pour mettre en œuvre une démarche pédagogique (“pair programming”, “remote laboratory”…)

<br/>
### Inscription à l'atelier

!!! tip ""
    L'inscription à l'atelier devra être réalisé via le [site de la conférence EIAH 2023](https://eiah2023.sciencesconf.org/). 

<br/>
### Calendrier et soumission
!!! tip ""
    - Date limite de soumission des contributions : 31 mars 2023
    - Date de notification aux auteur·e·s : 02 mai 2023
    - Dépôt de la version définitive : 26 mai 2023
    - Publication sur ce site : 09 juin 2023
    - Date de l'atelier dans le cadre d'EIAH'23 : mardi 13 juin (demi-journée)

<br/>
### Informations aux auteur·e·s

!!! tip ""
    Les soumissions à l'atelier devront utiliser le style de l'atelier (disponible dans l'archive [disponible ici](apimu_styles.zip) et contenant les patrons en différents formats : odt, docx, tex). En fonction de la nature de votre contribution, merci de rédiger un article compris entre 4 et 8 pages au maximum. <br/><br/>Les soumissions peuvent être anonymes, mais ce n'est pas une obligation. <br/><br/>Une fois mis en page avec le style de l'atelier, vos soumissions devront être téléversées sur l'espace *easychair* de l'atelier à l'adresse [https://easychair.org/conferences/?conf=apimueiah2023](https://easychair.org/conferences/?conf=apimueiah2023).

<br/>
### Programme de l'atelier : 9h - 10h30 11h - 12h30

!!! tip ""
    Communications :

    * [Mathieu Muratet, SPY : Un jeu sérieux partagé pour étudier l’apprentissage de la pensée informatique](pdf-apimu23/APIMUEIAH_2023_paper_1.pdf), [diaporama](pdf_eiah23/APIMUEIAH_2023_diapo_1.pdf)
    * [Sébastien Jolivet, Eva Dechaux, Anne-Claire Gobard et Patrick Wang, Description et analyse de trois EIAH d'apprentissage de Python](pdf-apimu23/APIMUEIAH_2023_paper_5.pdf), [diaporama](pdf_eiah23/APIMUEIAH_2023_diapo_5.pdf)
    * [Mirabelle Marvie-Nebut et Yvan Peter, Apprentissage de la programmation Python - Une première analyse exploratoire de l’usage des tests](pdf-apimu23/APIMUEIAH_2023_paper_2.pdf), [diaporama](pdf_eiah23/APIMUEIAH_2023_diapo_2.pdf)
    * [Emmanuel Desmontils et Laura Monceaux, Enseigner SQL en NSI](pdf-apimu23/APIMUEIAH_2023_paper_3.pdf), [diaporama](pdf_eiah23/APIMUEIAH_2023_diapo_3.pdf)
    * [Sophie Chane-Lune, Christophe Declercq et Sébastien Hoarau, Expérimentation du pair-programming en spécialité NSI en classe de première pour l’acquisition de compétences en programmation](pdf-apimu23/APIMUEIAH_2023_paper_4.pdf), [diaporama](pdf_eiah23/APIMUEIAH_2023_diapo_4.pdf)

    Table-ronde entre les participant.e.s de l'atelier sur le thème :

    * Expérimentations, analyses de traces et évaluation des compétences
<br/>
### Actes de la journée

!!! tip ""
    Les articles retenus pour présentation lors de la journée sont publiés dans les actes de l'atelier. Ces actes sont disponibles librement [ici](pdf-apimu23/proceedings-apimu23.pdf) et sur l'archive ouverte en ligne [HAL](https://hal.science).

<br/>
### Comité d'organisation

!!! tip ""
    * Yvan PETER (Université de Lille)
    * Yannick PARMENTIER (Université de Lorraine)
    * Yann SECQ (Université de Lille)
    * Julien BROISIN (Université de Toulouse)
    * Christophe DECLERCQ (INSPÉ de la Réunion)
    * Cédric FLUCKIGER (Université de Lille)

<br/>
### Comité scientifique (sous réserve d'acceptation)

!!! tip ""
    * Georges-Louis BARON (Université de Paris)
    * Florent BECKER (Université Orléans)
    * Charles BOISVERT (Université Sheffield)
    * Laure BOLKA-TABARY (Université de Lille)
    * Isabelle COLLET (Université Genève)
    * Kris COOLSAET (Universiteit Gent)
    * Yannis DELMAS (Université de Poitiers)
    * Brigitte DENIS (Université de Liège)
    * Liesbeth DE MOL (Université de Lille)
    * Fahima DJELIL (IMT Atlantique)
    * Béatrice DROT-DELANGE (Université de Clermont-Ferrand)
    * Marie DUFLOT-KREMER (Université de Lorraine)
    * Olivier GOLETTI (Université de Louvain)
    * Monique GRANDBASTIEN (Université de Lorraine)
    * Colin de la HIGUERA (Université de Nantes)
    * Sébastien JOLIVET (Université de Genève)
    * Bern MARTENS (Universiteit Leuven)
    * Kim MENS (Université de Louvain)
    * Gabriel PARRIAUX (HEP Lausanne)
    * Christophe REFFAY (Université Franche-Comté)
    * Robert REUTER (Université du Luxembourg)
    * Margarida ROMERO (Université Côte d'Azur)
    * Eric SANCHEZ (TECFA, Université de Genève)
    * Thierry VIÉVILLE (Inria Sophia Antipolis)
    * Amel YESSAD (Sorbonne Université)
