# Atelier "Apprentissage de la pensée informatique de la maternelle à l'Université : retours d'expériences et passage à l'échelle"

<br/>
## EIAH 2019 - 4 juin 2019 à Paris

<br/>
### Objectifs de l'atelier

Cet atelier fait suite aux ateliers organisés en 2017 lors des journées [Orphée RDV “apprentissage instrumenté de l'informatique”](https://apprentissageinstrumentdelinformatique.wordpress.com/), lors de la conférence [EIAH 2017](aii-eiah17.md) et lors des [RJC-EIAH](rjc-eiah18.md) en 2018.

Il vise à fédérer une communauté autour de la problématique de l'apprentissage de la pensée informatique et de la programmation, de la maternelle à l'université afin de bâtir une vision commune du domaine, en partager les problématiques et envisager les solutions possibles. Ces apprentissages font une entrée massive dans l'enseignement pré-bac en France. On observe des mouvements similaires à des degrés divers en Suisse, en Belgique… D'autre pays sont déjà bien engagés dans cette étape de formation de masse tels que la Grande-Bretagne ou l'Italie.

Cet atelier sera co-organisé pour la première fois avec le projet [ANR IE CARE](http://iecare.lip6.fr/) (enseignement de l'informatique à l'école obligatoire, 3-16 ans) ainsi qu'avec l'[IFIP TC3 - ICT and Education](http://www.ifip-tc3.net/) offrant l'opportunité d'un regard croisé sur les enjeux de cette massification de l'enseignement, à travers les retours d'expériences des participants à l'atelier. Nous espérons notamment des contributions sur la mise en œuvre de ces apprentissages dans la classe et leur évaluation, la formation et l'accompagnement des enseignants.

Nous profiterons de l'atelier pour initier la rédaction d'un article de synthèse basé sur les différentes contributions pour une soumission à la revue STICEF et / ou Education and Information Technologies Journal. Cet atelier sera également l'occasion d'une première mise en commun de ressources dans le cadre du GT Enseigner l'informatique de la maternelle à l'université de l'ATIEF.

D'une manière générale, les contributions pourront s'inscrire dans les thématiques suivantes :

- Apprendre / Enseigner la pensée informatique et la programmation.
- Former et accompagner les enseignants.
- Produire, évaluer et diffuser des outils et des ressources éducatives.

<br/>
### Programme de l'atelier

* 08h30-09h00 : Accueil

* 09h00-09h15 : Introduction de la journée

* 09h15-10h45 : **Formation à la pensée informatique** (90 min)

    - *Peut-on former les enseignant·e·s en un rien de temps ?* (20+10min) MARIAIS Christelle, ROCHE David, FARHI Laurence, BARNABE Sabrina, CRUCHON Sonia, DE QUATREBARBES Sophie, VIEVILLE Thierry ([Article](pdf-eiah19/peut-on_former_les_enseignant-e-s_en_un_rien_de_temps.pdf))

    - *Initiation à la Pensée Computationnelle avec JavaScript : le Cours STIC I* (10+10min) FRITZ Mattia, SCHNEIDER Daniel K. ([Article](pdf-eiah19/initiation_a_la_pensee_computationnelle_avec_javascript___le_cours_stic_1.pdf))

* 10h35-10h50 : Pause

* 10h50-11h20 : C. DECLERCQ & P. MARQUET - [*DIU Enseigner l'Informatique au Lycée : genèse et point d'étape*](https://www.societe-informatique-de-france.fr/wp-content/uploads/2019/04/1024-numero-13_Article9.pdf)

* 11h20-12h : Échanges/débat sur la formation à la pensée informatique

* 12h-13h30 : Repas

* 13h30-14h20 : **Apprentissage instrumenté de la PI - partie 1** (50min)

    - *Captation Automatisée et Visualisation de Traces de Programmation dans un Environnement de Programmation Graphique par Blocs* (20min+10min) LEGRAND Jean-Marc ([Article](pdf-eiah19/captation_automatisee_et_visualisation_de_traces_de_programmation_dans_un_environnement_de_programmation_graphique_par_blocs.pdf))

    - *Retour d'expérience sur l'utilisation du système AI-VT générant des listes personnalisées et variées d'exercices* (10min+10min) GREFFIER Françoise, HENRIET Julien
    
* 14h20-15h : **JUMP, une gazette par et pour les enseignants d'informatique de la maternelle à l'université**

* 15h-15h20 : pause

* 15h20-16h10 : **Apprentissage instrumenté de la PI - partie 2** (50min)

    - *Vers un Support à l'Entraide dans les Laboratoires Distants pour l'Apprentissage de l'Informatique* (20min+10min) VENANT Rémi, BROISIN Julien ([Article](pdf-eiah19/vers_un_support_a_l-entraide_dans_les_laboratoires_distants_pour_l-apprentissage_de_l-informatique.pdf))
    
    - *Évaluation de l'acceptabilité, de l'utilité et de l'utilisabilité du tableau de bord du jeu “Programming Game”* (10min+10min) PLUMETTAZ-SIEBER Maud, JACCARD Dominique, HULAAS Jarle, SANCHEZ Eric ([Article](pdf-eiah19/evaluation_de_l-acceptabilite_l-utilite_et_l-utilisabilite_du_tableau_de_bord_du_jeu_programming_game.pdf))
    
* 16h10-17h00 : **Table ronde animée par Eric BRUILLARD : “Quelles pratiques pour une meilleure capitalisation des retours d'expériences et l'émergence d'une communauté de pratiques en lien avec les recherches en didactique de l'informatique”**

    - *Apprentissage de la pensée informatique : de la formation des enseignants à la formation de tout·te·s les citoyen·ne·s.* GIRAUDON Gérard, LEFEVRE Saint-Clair, DE QUATREBARBES Sophie, MASSE Bastien, ROMERO Margarida, VIEVILLE Thierry ([Article](pdf-eiah19/apprentissage_de_la_pensee_informatique_de_la_formation_des_enseignants_a_la_formation_de_tout-te-s_les_citoyen-ne-s.pdf))

    - *Quels scénarios pour l'enseignement de l'informatique à l'école élémentaire ? Premiers éléments relatifs au contenu et au modèle des scénarios dans le cadre du projet IE-CARE.* BRUNET Olivier, YESSAD Amel, MURATET Matthieu, CARRON Thibault

    - *Enseigner la programmation Scratch en amatrice.* BALLAND Cassandra, BROUSSEAU Salomé, COAT Morgane, CONSTANTIN Laurie-May, CORAIN Layla, DANIEL Tinaïg, EL BOUTI Meryem, HERGOUALC'H Louise, KARCHAOUI Ikhlass, KERVOT Gwendoline, LIDEC Audrey, MACHARD Alix, PIERRY Océane, RIO Constance, ROGARD Caroline, SEITHERS Anaëlle, SINILO Maëlle, PLAUD Cécile, RIBAUD Vincent ([Article](pdf-eiah19/enseigner_la_programmation_scratch_en_amatrice.pdf))

* 17h-17h15 : Clôture de la journée

<br/>
### Soumissions

Nous attendons des articles de 4 à 8 pages en PDF au format de la conférence EIAH (LNCS) (modèle au format .docx ou Latex). Les articles feront l'objet d'une double relecture par les membres du comité scientifique. Les articles peuvent être soumis en français ou en anglais. Pour les articles acceptés, la présentation pourra être faite en Français ou en Anglais. Toutefois, pour les articles en Français, le support de présentation devra être en anglais pour faciliter la compréhension de tous.

Les articles seront à soumettre à l'adresse [ct2@univ-lille.fr](mailto:ct2@univ-lille.fr)

<br/>
### Dates importantes

- Soumissions : 31 mars 2019
- Avis d'acceptation : 26 avril 2019
- Versions finales : vendredi 24 mai 2019

<br/>
### Comité d'organisation

- Christophe Reffay, Université de Franche-Comté, France
- Fahima Djelil, Université de Haute Alsace, Mulhouse
- Maud Plumettaz-Sieber, Université de Fribourg, Suisse
- Yvan Peter, Université de Lille, France
- Yann Secq, Université de Lille, France
- Julien Broisin, Université de Toulouse, France

<br/>
### Comité scientifique

- Yvan Peter, Université de Lille, France
- Christophe Reffay, Université de Franche-Comté, France
- Ivan Kalas, Comenius University, Slovakia
- Yoshiaki Matsuzawa, Aoyama Gakuin University, Japan
- Valentina Dagiene, Vilnius University, Lithuania
- Fahima Djelil, Université de Haute Alsace, France
- Miroslava Černochová, Charles University, Czech Republic
- Olivier Goletti, Université Catholique de Louvain, Belgique
- Yann Secq, Université de Lille, France
- Julien Broisin, Université de Toulouse, France
- Julien Gossa, Université de Strasbourg, France
- Valérie Brasse, IS4RI / Graine2Tech, France
- Mary Webb, King's College London
- Cédric Fluckiger, Université de Lille, France
- Martin Quinson, ENS Rennes, France
- Eric Bruillard, Université Paris 5 (Descartes), France
