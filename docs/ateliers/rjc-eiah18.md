# Atelier "Organisation et suivi des activités d'apprentissage de l'informatique : outils, modèles et expériences"

<br/>
## RJC-EIAH 2018 - 6 avril 2018 - Besançon

<br/>
### Objectifs de l'atelier

Cet atelier fait suite aux ateliers organisés en 2017 lors des journées Orphée RDV ([apprentissage instrumenté de l'informatique](https://apprentissageinstrumentdelinformatique.wordpress.com/)) et de la conférence EIAH 2017 ([Atelier EIAH](aii-eiah17.md)). Il vise à fédérer une communauté autour de la problématique de l'apprentissage de la pensée informatique de la maternelle à l'université.
De nombreuses études ont démontré la difficulté de l'apprentissage de la programmation (e.g. [1]). Dans ce contexte, la capacité à accompagner les apprenants et leur fournir des rétroactions pertinentes au bon moment est critique [2]. Cette problématique se heurte toutefois à la variété des besoins des apprenants et à leur nombre. Elle prend une acuité particulière avec l'extension de l'apprentissage de la pensée informatique à l'école et plus généralement à des publics qui n'ont pas vocation à devenir informaticiens [3][4].
Cet atelier contribuera à recenser les pratiques et les outils existants dans la communauté ainsi que les problématiques propres à ce domaine. Il sera également l'occasion de préparer la création d'un groupe de travail de l'ATIEF sur ce sujet.

<br/>
### Références

[1] Lahtinen, E., Ala-Mutka, K., Järvinen, H.M. : A study of the difficulties of novice programmers. In : Proceedings of the 10th annual SIGCSE conference on Innovation and technology in computer science education - ITiCSE '05. p. 14. ACM Press, New York, New York, USA (2005) [https://doi.org/10.1145/1067445.1067453](https://dl.acm.org/doi/10.1145/1067445.1067453)

[2] Gutiérrez Rojas, I., Crespo García, R. M., & Delgado Kloos, C. (2012). Enhancing orchestration of lab sessions by means of awareness mechanisms. In 7th European Conference of Technology Enhanced Learning: 21st century learning for 21st century skills (pp. 113–125). [http://doi.org/10.1007/978-3-642-33263-0](http://doi.org/10.1007/978-3-642-33263-0)

[3] Baron, G.-L., Drot-Delange, B., Grandbastien, M., & Tort, F. (2014). Computer Science Education in French Secondary Schools. ACM Transactions on Computing Education, 14(2), 1–27. [http://doi.org/10.1145/2602486](http://doi.org/10.1145/2602486)

[4] Lye, S. Y., & Koh, J. H. L. (2014). Review on teaching and learning of computational thinking through programming: What is next for K-12? Computers in Human Behavior, 41, 51–61. [http://doi.org/10.1016/j.chb.2014.09.012](http://doi.org/10.1016/j.chb.2014.09.012)

<br/>
### Thématiques de l'atelier

Les précédents ateliers ont été l'occasion de recenser les outils existants et d'interroger la notion de « pensée informatique ».

**Pour cet atelier nous souhaitons nous concentrer sur les problématiques liées à l'organisation, l'orchestration et le suivi des activités d'apprentissage.**

Que ce soit en classe à l'école primaire, ou à l'université en salle de TP, l'enseignant se trouve confronté à la difficulté d'avoir une vision claire de l'avancement de ses élèves et aux moyens de les accompagner au mieux. Nous sollicitons donc des contributions sur ces thématiques. Nous espérons des contributions sur les propositions de modèles et outils mais également des retours d'expérience d'enseignants sur la façon dont ils abordent ces problématiques.

Les thématiques potentielles pour l'atelier sont :

- Environnements numériques pour l'apprentissage de la pensée informatique : micro-mondes, tuteur intelligent, jeux sérieux, robotique, interfaces tangibles…

- Outils et visualisations pour le suivi et l'analyse des apprentissages (à destination des apprenants, enseignants, tuteurs, concepteurs, etc.)

- Modélisation et organisation des activités, séquences et scénarios pédagogiques

- Outils de remédiation, de rétroaction, de régulation pour l'enseignant et/ou l'apprenant

- Retours d'expériences sur des séquences pédagogiques utilisées en classe ou en TP, la dimension collaborative, les outils utilisés et leurs effets, etc.
didactique de l'informatique, activités d'apprentissage de la pensée informatique

<br/>
### Soumissions

Nous attendons des articles de 4 à 8 pages en PDF au format de la conférence EIAH (LNCS) pour les articles scientifiques (modèle au format .docx ou Latex). Les retours d'expériences par des enseignants pourront être au format libre sur 4 pages maximum. Les articles feront l'objet d'une double relecture par les membres du comité scientifique.

Les articles seront à soumettre à l'adresse [ct2@univ-lille1.fr](mailto:ct2@univ-lille1.fr)

<br/>
### Dates importantes

- soumission : 16 février 2018
- notification d'acceptation : 26 février 2018
- relectures détaillées : 12 mars 2018
- version définitive des articles : 26 mars 2018

<br/>
### Déroulement

Les participants à l'atelier devront s'inscrire au RJC-EIAH (tarif préférentiel juqu'au 28 février 2018) : [page d'inscription](http://atief.fr/sitesConf/rjceiah2018/inscription.html)

L'atelier s'organisera en deux temps : une session de présentation des différentes contributions sur les propositions et retours d'expériences suivie de deux session de travail destinées à établir une proposition de groupe de travail dédié au sein de l'ATIEF et à développer un D(I)U Enseigner l'Informatique au Lycée.

* 9h00-9h10	Introduction

* 9h10-10h30	**Session “Université”** (3x(20'+6' questions))

    - F. Silvestre, J.-B. Raclet - *PRaTDL : un protocole fondé sur le test et la revue de code pour l'apprentissage de la programmation* ([Article](pdf-rjc-eiah18/franck-silvestre-jean-baptiste-raclet.pdf) / [Slides](pdf-rjc-eiah18/pratdl-rjc-eiah.pdf))

    - C. Boisvert - *Teaching relational database fundamentals: a lack-of-progress report* ([Article](pdf-rjc-eiah18/charles-boisvert.pdf))

    - Cédric Donner - *Des messages d'erreur compréhensibles de l'IDE TigerJython et diverses plateformes de programmation en ligne pour une meilleure gestion des cours de programmation* ([Article](pdf-rjc-eiah18/cedric-donner.pdf) / [Quelques liens sur TigerJython](http://www.tinyurl.com/eiah-info))

* 10h30-10h40	Pause

* 10h40-12h	**Session “Ecole”** (3x(20'+6' questions))

    - C. Declercq, F. Tort - *Organiser l'apprentissage de la programmation au cycle 3 avec des activités guidées et/ou créatives* ([Article](pdf-rjc-eiah18/christophe-declercq-francoise-tort.pdf) / [Slides](pdf-rjc-eiah18/declercqtort6avril2018.pdf))

    - Y. Parmentier - *Enseigner la pensée informatique à l'école primaire : formation initiale et continue des professeurs* ([Article](pdf-rjc-eiah18/yannick-parmentier.pdf) / [Slides](pdf-rjc-eiah18/pres-rjc-eiah-18-yannick-parmentier.pdf))

    - V. Ribaud et al. - *Apprentissage par projets à l'école primaire avec les filles qui...*  ([Article](pdf-rjc-eiah18/al-et-vincent-ribaud.pdf))
    
* 12h-13h30	Repas

* 13h30-14h50	Travail de groupe : **vers la création d'un GT au sein de l'ATIEF ?**

* 15h10-16h30	Travail de groupe : **vers la création de D(I)U Enseigner l'Informatique au Lycée (puis au Collège et à l'École) ?**

<br/>
### Membres du comité scientifique

- Souleiman Ali Houssein (U. Djibouti/U. Lille)
- Charles Boisvert (U. Sheffield Hallam)
- Valérie Brasse (IS4RI)
- Julien Broisin (U. Toulouse)
- Peter Dawyndt (U. Ghent)
- Fahima Djelil (UHA)
- Olivier Goletti (UCL)
- Julien Gossa (U. Strasbourg)
- Yvan Peter (U. Lille)
- Martin Quinson (ENS Rennes)
- Yann Secq (U. Lille)
- Rémi Venant (U. Toulouse)

<br/>
### Organisateurs de l'atelier

- Julien Broisin, E-C en Informatique à l'Université Paul Sabatier de Toulouse 3
- Peter Dawyndt, Department of Applied Mathematics, Computer Science and
Statistics, Ghent University, Belgique
- Olivier Goletti, ICTM, Université catholique de Louvain
- Julien Gossa, E-C en Informatique à l'Université de Strasbourg
- Yvan Peter, E-C en Informatique à l'Université de Lille - Sciences & Technologies
- Yann Secq, E-C en Informatique à l'Université de Lille - Sciences & Technologies
