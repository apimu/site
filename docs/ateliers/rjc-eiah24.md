# Atelier "Modélisations de l’activité de programmation (savoirs et savoir-faire, compétences, capacités…) : de quelles manières ? pour quels besoins ? pour quels usages ?"

## RJC-EIAH - 4 juin 2024, Laval

### Descriptif de l'atelier

!!! tip ""
    Cet atelier, est organisé conjointement par les Groupes de Travail Compétences et EIAH et APIMU (Apprentissage de l'Informatique de la Maternelle à l'Université) de l'[ATIEF](http://www.atief.fr).
    
    Comprendre ce qu’est l’activité de programmation est un questionnement ancien (Pennington & Grabowski, 1990). Diverses approches et motivations ont guidé ce questionnement (Guzdial & Du Boulay, 2019) : quelle est l’activité cognitive ? Quelles sont les compétences de programmation ? Quels sont les savoirs et savoir-faire liés à l’apprentissage de la programmation ?

    Diverses propositions pour modéliser et représenter ces éléments existent déjà : référentiels de compétences, référentiels de praxéologies, modèle COMPER, etc. (Grévisse et al., 2017; Jolivet et al., 2023; Rogalski & Samurçay, 1990)

    Les motivations menant à la production de telles modélisations sont aussi très diverses : objectifs orientés métier / formation ; exploitations dans un EIAH (adaptive learning…) ; outils pour l’enseignement ; outils d’analyse – description de ressources ; etc. Ces objectifs différents amènent des réponses diverses : niveau de modélisation, grain des compétences…

    Cet atelier a pour objectif de mettre en regard ces différents travaux, d’origines et de motivations diverses, pour essayer d’identifier les convergences possibles (formalisation des référentiels ; processus de construction ; articulations entre des référentiels de niveaux de granularité différents ; etc.).

    Il se déroule sur deux demi-journées. Il est possible de participer uniquement le matin, par contre pour participer l'après-midi il est cohérent d'avoir suivi la matinée. La matinée sera consacrée à la présentation de différents travaux impliquant une modélisation et/ou une utilisation de l'activité de programmation (présentation détaillée des interventions à venir). L'après-midi sera organisée en deux temps :

    - Une introduction avec des retours sur les présentations réalisées le matin selon deux axes : "quelle modélisation des connaissances dans les différents travaux ?"" et "quelle place pour les compétences dans les différents travaux ?".
    - Un temps de partage et d'échanges : existe-t-il des perspectives commmunes ? Quelles concrétisations ?

### Inscription à l'atelier

!!! tip ""
    L'inscription à l'atelier devra être réalisé via le [site de la conférence RJC-EIAH 2024](https://rjc2024.sciencesconf.org/). Attention tarif majoré après le 19 avril.

### Programme de l'atelier :  matinée 8h30 - 9h45 / 10h15 - 11h30

!!! tip ""
    Présentations

    - [Référentiel de compétences en algorithmique et programmation au cycle 4](pdf-rjc-eiah24/grenoble.pdf) (Equipe MeTAH & IREM Grenoble, Grenoble ; Emmanuel Beffara et al)
    - [Reconnaissance de motifs et notion de répétition](pdf-rjc-eiah24/lille.pdf) (Equipe CRIStAL, Lille ; Yvan Peter ; Marielle Leonard ; Yann Secq)
    - [Construction (et exploitation) d’un référentiel de types de tâches d’apprentissage de la programmation](pdf-rjc-eiah24/geneve_vaud.pdf) (Equipe TECFA - HEP Vaud, Suisse, Sébastien Jolivet & Patrick Wang)
    - [RCP : un référentiel de compétences en programmation](pdf-rjc-eiah24/la_reunion.pdf) (Equipe LIM Réunion ; Sophie Chane-Lune, Christophe Declercq, Sébastien Hoarau)
    - [Construction, exploitation et reconstruction d’un référentiel de compétences pour l’enseignement de la programmation récursive en Licence 1](pdf-rjc-eiah24/lyon.pdf) (Equipe COMPER au LIRIS, Lyon ; Nathalie Guin et Marie Lefevre)
    - [Échanges avec des enseignants autour des compétences du PIAF dans le cadre du jeu sérieux SPY](pdf-rjc-eiah24/sorbonne.pdf) (Equipe MOCAH, LIP6, Sorbonne Université ; Mathieu Muratet)
    - [A l’interface entre la recherche et les salles de classe](pdf-rjc-eiah24/vaud.pdf) (Equipe HEP Vaud, Morgane Chevalier, Engin Bumbacher, Patrick Wang)

### Programme de l'atelier : après-midi 13h30 - 14h45 / 15h15 - 17h00

!!! tip ""
    1. Synthèse des présentations du matin
    
        - Marie Lefevre et Amel Yessad : [quelles ingénieries des connaissances ?](pdf-rjc-eiah24/synthese_ingenierie_connaissances.pdf)
        - Nathalie Guin : [et les compétence dans tout ça ?](pdf-rjc-eiah24/synthese_competences.pdf)
        - Sébastien George : [quels aspects programmation ?](pdf-rjc-eiah24/synthese_programmation.pdf)
        

    2. Echanges et perspectives :

        - Mutualisation des processus de production
        - Partage - mutualisation des existants (moyens et outils ?)
        - Perspectives communes

    [Synthèse présentée en plénière des RJC-EIAH](pdf-rjc-eiah24/synthese_RJC.pdf)

### Organisateurs

!!! tip ""
    * Sébastien Jolivet (IUFE & TECFA, Université de Genève)
    * Yvan PETER (Université de Lille)
