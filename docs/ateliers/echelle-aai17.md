## Groupe "passage à l'échelle"

- Groupe passage à l'échelle pour accompagner l'introduction de la pensée informatique dans le système scolaire (carte mentale: [https://framindmap.org/c/maps/302947/public](https://framindmap.org/c/maps/302947/public) )

- Quelques éléments sur le “volume”:

    - 86100 enseignants dans les écoles (primaires?) + collège + lycées
    
    - 71 universités (en 2016) et 4200 lycées (dont 1500 pro), 7100 collèges (rentrée 2015) et 51700 écoles élémentaires

- Besoin d'aide technique pour monter en charge sur la formation des enseignants: apprentissage instrumenté de l'informatique

    1. Travailler pour les gens qui vont faire l'enseignement (les profs, les asso)
        - Adapter les contenus ? c'est un peu hors sujet ici, mais n primaire, c'est 80% de littéraires. Experiences au Canada pour s'appuyer sur le conte
        
        - Aider les profs à gérer les jeunes qui en savent plus que les profs
        
        - En EPS, les profs ont l'habitude de gérer le fait que les élèves courent plus vite qu'eux
        
        - En EPS, les fondamentaux sont d'assurer que les élèves ne se blessent pas, et qu'ils progressent quel que soit leur niveau
        
        - Comment aider les enseignants à évaluer le niveau des élèves s'ils ont pas le dit niveau. Il faut des cadres de référence
        
        - Un cadre de référence n'est pas un outil. On peut vouloir un cadre unifié, servi par des outils diversifiés

    2. Travailler organiser les outils en tant que tels
    
        - Est-ce qu'on peut faire converger les outils, et/ou les cartographier pour simplifier le choix
        
        - Travailler à l'interface avec les ENT pour rendre ces outils faciles d'accès (standardiser les interfaces pour permettre la diversité des offres)
        
        - Il faut que ces outils soient très solides : abandon au moindre pb technique en classe

    3. Travailler avec l'Education Nationale pour la formation
    
        - Organiser la communauté des producteurs
       
              - Outils à la curation de ressources (+organisation/structuration)
              
              - Mécanisme de recommendation à la fois généralisé et décentralisé
              
              - Approche crowd-sourcé vs les manuels d'éditeurs professionnels, mais c'est centralisable avec des interfaces unifiées dans les ENT

        - Aider à la formation des formateurs
       
            - En formation continue des enseignants, Il faut dédramatiser l'informatique auprès des enseignants, PUIS lui permettre de l'enseigner
              
            - L'auto-formation ne suffit pas. On peut s'appuyer sur les étudiants, les pros de l'IT, les E-C de l'informatique
              
            - Intervenir au niveau institutionnel (dans les ESPE, au niveau capes maths-info, au niveau des agregs option info)
              
            A. Impact de l'organisation de l'Education Nationale et particulièrement le fonctionnement du Rectorat et le rapport des enseignants à cette hiérarchie
              
            B. class'code sur Magistere (système du Rectorat)

       - Travailler au niveau des élus et particulièrement du Ministère (sans impulsion politique les résistances)

     4. Impliquer le secteur privé (mécénat de compétence par exemple, construction de ressources éducatives libres)

- Inventer des nouveaux outils (cf. curation, recommandation, crowd-source) et travailler à la convergence des outils existants

#### Trois aspects à creuser

1. fonctionnel: grille lecture type “il faut que l'outil permette aux apprenants de commenter les solutions des autres”, “il faut pouvoir comparer l'appropriation d'un concept à partir des traces de l'apprenant”),

2. conceptuel: concepts/compétences que l'on vise à faire approprier (notion de séquence, d'exécution, de répétition, de récursivité, d'ordre/de tri … savoir décomposer un problème en sous-problème, savoir identifier l'information pertinente)

3. technique: API REST/JSON pour rendre des exercices intégrables dans d'autres systèmes (cf. question du déploiement via les navigateurs = zéro installation) et pour fournir le contexte d'appropriation pour les enseignants/formateurs

#### Objectifs

1. fournir un point d'entrée reconnu par les différentes tutelles considérées comme prescriptrices pour les enseignants (sociétés savantes, sociétés professionnelles, laboratoires de recherche, ESPE (?!), influenceurs (café pédagogique), syndicats professionnels (SYNTEC))

2. fournir un ensemble de séquences pédagogiques pouvant être présentées sous différentes formes:

    - navigation par “niveau scolaire”, ie. les cycles 1, 2, 3 … ⇒ faciliter l'appropriation par les PE et profs de collège
    
    - navigation par concept: notion de répétition, notion d'alternative, plus général notion de structure de contrôle

3. pour chaque séquence, fournir à la fois une séquence pédagogique directement exploitable (accessible d'un clic via un navigateur) et le tutoriel à destination des encadrants expliquant les notions mises en oeuvre dans cette séquence

4. définir une “ontologie” minimale des traces d'activité des apprenants (au niveau des élèves/étudiants, mais aussi des médiateurs/formateurs/enseignants) et de l'usage des différentes séquences pédagogiques et fournir un accès à ces traces pour réaliser différentes évaluations (exemple: temps passé par une cohorte d'élèves sur certaines séquences pédagogiques, caractérisation de l'appropriation de certains concepts par détermination d'un pourcentage de la cohorte ayant réussi une certaine séquence (en un certain temps ?), nombre d'instanciation de la séquence en terme de nombre de classes/nombre d'élèves, temps passé par les encadrants sur les tutoriels décrivant la séquence des élèves (avec des mécanismes minimaux tels que des QCM ?), … ),

5. faciliter la réutilisation de “grains pédagogiques” au niveau des ressources de formation des encadrants (cf. vidéo courtes sur un concept donné)

6. construire un réseau impliquant la diversité des acteurs: enseignants, inspecteurs, élus, monde associatif, entreprises “du numérique”,

7. question de la diversité de la précision des traces d'activités des apprenants: soit totalement anonyme et portant uniquement sur l'usage des outils (cf. martin/PLM, accessible à tous), avec des données “institutionnelles” mais pas “individuelles” (tel collège, telle classe) accessibles via une autorisation via le Rectorat et pour les données “individuelles” (sexe, age) accès restreint sévèrement (cf. INSEE et accès sur un poste après accréditation …)

8. construction d'une évaluation type agile ? cf. questionnaire en amont, séquence pédagogique, questionnaire en aval, montrer l'appropriation d'un concept en “mesurant” sa transposition dans différents contextes. Exemple: détection de redondance et synthèse sous forme de répétition: amont (partition de musique), séquence (déplacement d'un personnage + tortue), aval (brodeuse programmable)

#### Questions de recherche

1. comment classifier les ressources ?

2. définir un moteur de recherche ressources

3. permettre aux enseignants de faire du suivi différencié

4. permettre aux enseignants de collaborer entre eux

5. fournir des ressources pédagogiques à la fois clé en main et modifiables par les enseignants

6. identifier les connaissances et compétences à faire acquérir aux élèves/étudiants et imaginer les situations dans lesquelles elles peuvent être acquises

#### Les avantages du domaine de l'informatique

1. il y a des productions et des traces numériques et facilement exploitables de fait

2. il existe déjà des outils permettant d'analyser ces productions, même si initialement ils ne sont pas destinés à cela (exemple : sonar pour analyser du code)

3. beaucoup d'outils pour l'apprentissage par l'action (facilite l'évaluation des compétences ?), les systèmes ne sont pas des simulateurs éloignés de la réalité (différent des STEM en général)

#### Les inconvénients du domaine

1. enseignement peu structuré (pas de CAPES/agreg dans le domaine)
