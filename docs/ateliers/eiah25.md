# Atelier "Apprentissage de la pensée informatique de la maternelle à l'Université : APIMU 1000"

<br/>
## EIAH 2025 - Mardi 10 juin 2025, Lille

<br/>
### Descriptif de l'atelier

!!! tip ""
Pour cette 1000eme édition de l'atelier APIMU, il est temps de faire le bilan de 20 ans de recherches autour de la pensée informatique (depuis l'article fédérateur de Jeannette Wing) et de son apprentissage de la maternelle à l'université. Quels instruments, quels dispositifs ont été développés pour l'apprentissage de la pensée informatique ? Les objectifs de la "pensée informatique" sont-ils présents dans les curricula et dans la formation des enseignantes et enseignants ?
Quelles modélisations de l'activité de l'élève, de ses compétences, de ses représentations ?
Comment s'inscrit la “pensée informatique” dans la didactique de l'informatique et dans le prolongement des travaux en psychologie de la programmation ?

Cet atelier participatif propose de mettre en débat ces problématiques entre les participantes et participants de l'atelier après présentation de communications sélectionnées selon l'appel à contributions suivant.


<br/>
### Thèmes

!!! tip ""
    Les thèmes qui pourront être abordés incluent :
    
    * Instruments pour l'apprentissage de la pensée informatique : environnements collaboratifs, micro-mondes, tuteurs intelligents, jeux sérieux, robotique, interfaces tangibles, informatique sans ordinateur…
    * Modélisation de l'activité de l'élève, de ses compétences et de ses représentations
    * Collecte, mise à disposition et analyse des traces d'apprentissage pour le suivi, la remédiation, la rétroaction et/ou la régulation.
    * Formation des enseignantes et enseignants à l'enseignement de la pensée informatique
    * Genre et pensée informatique.

<br/>
### Inscription à l'atelier

!!! tip ""
    L'inscription à l'atelier devra être réalisé via le [site de la conférence EIAH 2025](https://eiah2025.sciencesconf.org/). 

<br/>
### Calendrier et soumission
!!! tip ""
    - Date limite de soumission des contributions : 30 mars 2025
    - Date de notification aux auteur·e·s : 02 mai 2025
    - Dépôt de la version définitive : 25 mai 2025
    - Publication sur ce site : 09 juin 2025
    - Date de l'atelier dans le cadre d'EIAH'25 : mardi 10 juin 2025 de 9h à 12h30

<br/>
### Informations aux auteur·e·s

!!! tip ""
    Les soumissions à l'atelier devront utiliser le style de l'atelier (disponible dans l'archive [disponible ici](apimu_styles.zip) et contenant les patrons en différents formats : odt, docx, tex). En fonction de la nature de votre contribution, merci de rédiger un article compris entre 4 et 8 pages au maximum. <br/><br/>Les soumissions peuvent être anonymes, mais ce n'est pas une obligation. <br/><br/>Une fois mis en page avec le style de l'atelier, vos soumissions devront être téléversées sur l'espace *easychair* de l'atelier à l'adresse [https://easychair.org/conferences/?conf=apimueiah2025](https://easychair.org/conferences/?conf=apimueiah2025).

<br/>
### Programme de l'atelier : 9h - 10h30 11h - 12h30

!!! tip ""
    Communications :

(à venir)

<br/>
### Actes de la journée

!!! tip ""
    Les articles retenus pour présentation lors de la journée seront publiés dans les actes de l'atelier. Ces actes seront disponibles librement sur le site [APIMU](http://apimu.org) et sur l'archive ouverte en ligne [HAL](https://hal.science).

<br/>
### Comité d'organisation

!!! tip ""
    * Yvan PETER (Université de Lille)
    * Yannick PARMENTIER (Université de Lorraine)
    * Yann SECQ (Université de Lille)
    * Julien BROISIN (Université de Toulouse)
    * Christophe DECLERCQ (Université de la Réunion)
    * Cédric FLUCKIGER (Université de Lille)

<br/>
### Comité de lecture

!!! tip ""
    * Julien BROISIN (Université de Toulouse)
    * Liesbeth DE MOL (Université de Lille)
    * Christophe DECLERCQ (Université de la Réunion)
    * Brigitte DENIS (Université de Liège)
    * Cédric FLUCKIGER (Université de Lille)
    * Sébastien JOLIVET (Université de Genève)
    * Yannick PARMENTIER (Université de Lorraine)
    * Yvan PETER (Université de Lille)
    * Robert REUTER (Université du Luxembourg)
    * Yann SECQ (Université de Lille)
    * Thierry VIÉVILLE (Inria Sophia Antipolis)

