# Atelier "Apprentissage Instrumenté de l'Informatique" - Rencontre ORPHEE-RDV 2017 (Font-Romeu)

!!! tip ""
    - [tour de table](qui-aai17.md): qui êtes-vous, que faites-vous et pourquoi êtes-vous venus ?
    - [groupe "passage à l'échelle"](echelle-aai17.md)
    - [groupe "évaluation des outils"](evaluation-aai17.md)
    - [groupe "au delà de l'algo-prog"](au-dela-aai17.md)
    - [ébauche de taxonomie des outils existants](taxonomie-aai17.md)
    - [spécifications fonctionnelles de notre chimère](chimere-aai17.md)

### Objectifs (réalistes ;)) pour construire à partir de cette première rencontre/ces premiers échanges

!!! tip ""
    - mise en place d'outils de communication/partage d'information (liste de diffusion + wiki: presque DONE ;))
    * article 2 pages sur Grand Challenge d'ici fin février
    * avancer sur des petits objectifs développant les interactions entre les membres du groupe sur des objectifs à court terme:
          * “survey/state of the art” sur les méthodes d'évaluation des outils
          * travail d'accumulation pouvant se faire en décentralisé/asynchrone sur wiki
          * ébauche de plan possible en distanciel et cristallisation en présentiel à court terme (avant rentrée prochaine)
          * recueil, classification des ressources disponibles/mobilisables (ie. produits pour info, mais pas si non appropriables)
          * et notamment les outils/expériences des membres du groupe :espace dans le wiki pour mettre la description de l'outil (+ liens) et y ajouter quelques références biblio en lien avec la problématique adressée par l'outil ou la catégorie d'outils.
          * premiers éléments d'une ontologie 'light' de description de ressources pédagogiques en informatique en prenant un sous-ensemble de LOM et autres ontologies existantes (dublin core ? …)
          * piste de site vitrine (cf. ligne 633)
          * élargir le cercle du groupe initié à ORPHEE-RDV'17, surtout en terme de discipline (peut-on dépasser le périmètre des informaticiens ?)
          * prendre contact avec des invités potentiels pour les rencontres en présentiel:
          <br/>François Villemonteix ? des participants de la table-ronde de DidaSTIC'16 ([https://didapro6.sciencesconf.org](https://didapro6.sciencesconf.org)) ⇒ table ronde (animation Éric Bruillard) : Questions et méthodes actuelles en didactique de l'informatique, un aperçu de la situation en francophonie - Georges-Louis Baron (Université de Paris Descartes, France), Gabriel Parriaux (HEPL Lausanne, Suisse), Étienne Vandeput (ex - UNamur et ULg, Belgique)
    * garder un oeil sur les appels à projets (ANR, PICS, H2020 …) qui pourraient soutenir l'organisation de temps de rencontre en présentiel + fournir un/des cadres pour des projets à moyen terme
    * équivalent d'un PISA en informatique ?
           * peut-on le faire sur 3 “niveaux”: entrée 6ème, entrée 2nd, entrée supérieur (cf. ci-dessous: concrètement 3 parcours avec l'infrastructure de Castor)
           * s'appuyer sur les résultats au BEPC de l'épreuve d'informatique entrant en vigueur en 2017
           * le concours CASTOR informatique ?
           <br/>- piste à creuser avec un ensemble restreint de problèmes à résoudre en temps limité pour des cohortes entrant à l'Université
           <br/>- terrains: L1 SESI Toulouse (800-1000!), DUT Info Lille (130) mobilisation d'autres dpt. via l'ACD ?
    * Nécessité site vitrine: quelques ressources “polies” et trois axes un centré activité connectées (peu d'implication pour l'appropriation), un centré formation de formateurs et le dernier sur les activités déconnectées (plus délicates à s'approprier sans un background informatique)
           * activités clés en main: pour l'instant, juste ces 3 “tests de positionnement”, conservation des traces, comparaison de cohortes tous les ans, extension du volume (relais via Castor ?)
           * intégration Class'Code: première initiation aux concepts (via OpenCC ou pas ? pb du suivi si pas … pixees peut gérer cela ? demander à T. Vieville)
           * intégration SMN: pas tout mais travail “marketing” objectif + vidéo + article type “wikipedia” éléments scientifiques et liens pour approfondir

## Points d'étape à court terme

!!! tip ""
    - temps potentiel de rencontre vers la mi-avril pour Charles qui sera de passage en France
    - proposer un atelier à EIAH ([http://eiah2017.unistra.fr](http://eiah2017.unistra.fr)) le mardi 06/06/17 + un temps hors cadre le mercredi 7/6/17 à l'UNISTRA
    - apprentissage instrumenté de l'informatique ⇒ repartir de la base appel ORPHEE
    - une ou deux présentations d'invités faisant dans la recherche dans le domaine depuis des années (Hubwieser ?) (pourrait peut-être apporter un regard sur les priorités que nous nous fixons)
    - proposer un atelier à Scratch Bordeaux ([http://www.scratch2017bdx.org](http://www.scratch2017bdx.org)) du 18 au 21/07/2017 (contact local à Bordeaux ?)
